#! /bin/bash

# Define a vanilla Debian 12 VM.
# The nocloud variant means it can run anywhere (and allows password-less root login).

# For reference, the domain was installed using the following command.
# At the time of writing, oslib on Debian 12 did not have an entry for Debian 12, so tell virt-install it is a Debian 11...
# This was manually fixed in the domain definition file.
# virt-install --import --name debian12-nocloud \
#     --osinfo debian11 \
#     --memory 102400 --vcpus 64 --cpu host \
#     --graphics none --autoconsole text \
#     --disk path=debian-12-nocloud-amd64.qcow2,bus=virtio,cache=directsync,driver.io=io_uring \
#     --network bridge=virbr0,model=virtio

SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"
source "$SCRIPTDIR/env"
source "$SCRIPTDIR/fancyterm.sh"

DEFINITION_TMPL_FN="$SCRIPTDIR/debian-12-nocloud.xml.tmpl"
DEFINITION_FN="$SCRIPTDIR/debian-12-nocloud.xml"
DOMAIN_NAME="$BASE_DOMAIN_NAME"
DOMAIN_IMAGE="$SCRIPTDIR/$BASE_DOMAIN_IMAGE"

print_title "defining domain $bold$DOMAIN_NAME$nobold"

if ! [ -f "$DOMAIN_IMAGE" ]; then
    if ! [ -f "$DEBIAN12_IMAGE" ]; then
        print_info "Debian 12 base image $bold\"$DEBIAN12_IMAGE\"$nobold not found, downloading"

        wget https://cloud.debian.org/images/cloud/bookworm/latest/"$(basename "$DEBIAN12_IMAGE")" ||
            die "failed downloading domain image from ${bold}https://cloud.debian.org/images/cloud/bookworm/latest/$(basename "$DEBIAN12_IMAGE")$nobold"
    fi

    print_info "domain image $bold\"$DOMAIN_IMAGE\"$nobold not found, creating"

    qemu-img create \
        -f qcow2 -b "$DEBIAN12_IMAGE" \
        -F qcow2 "$DOMAIN_IMAGE" ||
        die "failed creating base domain image $bold\"$DOMAIN_IMAGE\"$nobold from Debian 12 base image $bold\"$DEBIAN12_IMAGE\"$nobold"
fi

print_info "making definition file from template"

sed 's!{{ DOMAIN_NAME }}!'"$DOMAIN_NAME"'!
     s!{{ SCRIPTDIR }}!'"$SCRIPTDIR"'!
     s!{{ DOMAIN_IMAGE }}!'"$DOMAIN_IMAGE"'!' "$DEFINITION_TMPL_FN" >"$DEFINITION_FN" ||
    die "failed making definition file $bold\"$DEFINITION_FN\"$nobold from template $bold\"$DEFINITION_TMPL_FN\"$nobold"

print_info "defining domain $bold$DOMAIN_NAME$nobold"

libvirt_run virsh define "$DEFINITION_FN" ||
    die "failed defining domain from $bold\"$DEFINITION_FN\"$nobold"

print_info "preparing system to be a base for clones:"
printf "* reset machine ID\n"
printf "* mount directory shared with the host\n"

# * customize: append-line, copy
# * machine-id: remove machine ID
libvirt_run virt-sysprep --domain "$DOMAIN_NAME" \
    --operations customize,machine-id,script \
    --append-line "/etc/fstab":'host-sharing_tag	/mnt/host-sharing	virtiofs	defaults	0	0' \
    --copy-in "$SHARED_WITH_GUESTS_DP/rsztty":"/usr/bin" \
    --append-line "/root/.bash_rc":'rsztty' ||
    die "failed preparing clone system"

print_info "You can now clone this base machine into a base machine for FaaS nodes: run $bold\`\"$SCRIPTDIR/define-vm_faas0.sh\"\`$nobold."

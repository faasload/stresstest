#! /bin/bash

# Define the base VM for nodes of an OpenWhisk cluster.

SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"
source "$SCRIPTDIR/env"
source "$SCRIPTDIR/fancyterm.sh"

DOMAIN_NAME="$FAAS_BASE_DOMAIN_NAME"
DOMAIN_IMAGE="$DOMAIN_NAME.qcow2"

MAIN_IMAGE_PARTITION="/dev/sda1"

print_title "defining domain $bold$DOMAIN_NAME$nobold"

clone_domain "$BASE_DOMAIN_NAME" "$BASE_DOMAIN_IMAGE" "$DOMAIN_NAME" "$DOMAIN_IMAGE"

print_info "resizing main filesystem of guest image"

# The base Debian image is only 2GiB in virtual size, resize the root partition in the FaaS nodes to occupy the size set
# while cloning the base domain.
virt-resize --expand "$MAIN_IMAGE_PARTITION" \
    "$BASE_DOMAIN_IMAGE" "$DOMAIN_IMAGE" ||
    die "failed resizing filesystem of main partition $bold\"$MAIN_IMAGE_PARTITION\"$nobold in FaaS node image"

# * customize: firstboot-command
libvirt_run virt-sysprep --domain "$DOMAIN_NAME" \
    --operations customize \
    --firstboot-command "/mnt/host-sharing/install-kubernetes.sh"

print_subtitle "setup $DOMAIN_NAME as a base Kubernetes installation"

print_warn "Start this machine once with $bold\`\"$SCRIPTDIR/run-vm.sh\" $DOMAIN_NAME\`$nobold to install Kubernetes."
print_info "This will take some time while messing a bit with the display."
print_info "Then, just shutdown the VM (you can login as ${bold}root$nobold)."
print_info "Now, you shall clone this base machine into as many FaaS nodes as you wish:"
printf "run $bold\`\"%s/define-vm_new-faas-node.sh\"\`$nobold as many times as required.\n" "$SCRIPTDIR"

#! /bin/bash

SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"
source "$SCRIPTDIR/env"
source "$SCRIPTDIR/fancyterm.sh"

print_subtitle "running a FaaS node VM"

# ARGUMENTS {{{
# See usage message.
domain=""
console="--console"

echo_usage() {
    echo -e "Usage: $bold$red$0$reset VM [--no-console]"
    echo
    echo -e "Run a FaaS node virtual machine."
    echo
    echo -e "Arguments:"
    echo
    echo -e "\t$bold${green}VM$reset"
    echo -e "\t\tName of the virtual machine, i.e., of the libvirt domain."
    echo
    echo -e "Options (only long forms):"
    echo
    echo -e "\t$bold$red--no-console"
    echo -e "\t\tDo not attach to console after creation"
    echo
    echo -e "\t$bold$red--help$reset"
    echo -e "\t\tDisplay this help text."
}

while [ $# -gt 0 ]; do
    case "$1" in
    --*) # flags
        case "${1#--}" in
        "no-console")
            console=""

            print_info "not attaching to VM's console"

            shift 1
            ;;
        "help")
            echo_usage
            exit 0
            ;;
        *)
            print_err "unknown option \"$1\""
            echo
            echo_usage
            exit 2
            ;;
        esac
        ;;
    *) # arguments
        if [ -n "$domain" ]; then
            print_err "unexpected argument \"$1\""
            echo
            echo_usage
            exit 2
        else
            domain="$1"
            shift 1

            print_info "starting VM $bold$domain$nobold"
        fi
        ;;
    esac
done

if [ -z "$domain" ]; then
    print_err "missing argument VM"
    echo
    echo_usage

    exit 2
fi
# ARGUMENTS }}}

socket_path="$(printf "$VIRTIOFSD_SOCKET_PAT" "$domain")"

print_info "starting virtiofsd on socket $bold\"$socket_path\"$nobold"

"$VIRTIOFSD_EXE" --socket-path="$socket_path" \
    --shared-dir="$SCRIPTDIR/shared-with-faas-guests" >/dev/null &

print_info "starting domain"

libvirt_run virsh start $console "$domain" ||
    die "failed starting domain"

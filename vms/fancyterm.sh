#! /bin/bash

# Fancyterm -- Shell definitions to make your scripts fancy!

if tput colors >/dev/null 2>&1; then
    reset="$(tput sgr0)"

    bold="$(tput bold)"
    # tput does not have a default capability to turn bold off, but most often
    # it is 22.
    nobold="\e[22m"
    italics="$(tput sitm)"
    noitalics="$(tput ritm)"
    underline="$(tput smul)"
    nounderline="$(tput rmul)"
    # standout is essentially reverse video as it seems.
    standout="$(tput smso)"
    nostandout="$(tput rmso)"

	black="$(tput setaf 0)"
    red="$(tput setaf 1)"
    green="$(tput setaf 2)"
    yellow="$(tput setaf 3)"
    blue="$(tput setaf 4)"
    magenta="$(tput setaf 5)"
    cyan="$(tput setaf 6)"
    white="$(tput setaf 7)"
fi

_FT_extra_cr_printed=false

print_fancy_title() {
    [ $_FT_extra_cr_printed = false ] && printf "\n"
    printf "${magenta}${bold}${underline}#{ ${nounderline}${standout}%b${nostandout}${underline} }#$reset\n" "$*"
    printf "\n" && _FT_extra_cr_printed=true
}

print_title() {
    [ $_FT_extra_cr_printed = false ] && printf "\n"
    printf "${cyan}${standout}### %b$reset\n" "$*"
    printf "\n" && _FT_extra_cr_printed=true
}

print_subtitle() {
    [ $_FT_extra_cr_printed = false ] && printf "\n"
    printf "${cyan}### %b$reset\n" "$*"
    _FT_extra_cr_printed=false
}

print_info() {
    printf "${blue}INF %b$reset\n" "$*"
    _FT_extra_cr_printed=false
}

print_warn() {
    printf "${yellow}WAR %b$reset\n" "$*"
    _FT_extra_cr_printed=false
}

print_err() {
    printf "${red}ERR %b$reset\n" "$*" 1>&2
    _FT_extra_cr_printed=false
}

die() {
    print_err "$*"
    exit 1
}

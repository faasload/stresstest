#! /bin/bash

# Define a new VM to serve as a node of an OpenWhisk cluster.

SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"
source "$SCRIPTDIR/env"
source "$SCRIPTDIR/fancyterm.sh"

print_title "defining new FaaS node"

last_domain_name="$(libvirt_run virsh list --all --name |
    grep "$FAAS_DOMAIN_BASENAME" | sort | tail --lines=1)"
last_domain_num="${last_domain_name#"$FAAS_DOMAIN_BASENAME"}"

domain_name="$FAAS_DOMAIN_BASENAME$((last_domain_num + 1))"
domain_image="$SCRIPTDIR/$domain_name.qcow2"

print_info "cloning base FaaS node domain $bold\"$FAAS_BASE_DOMAIN_NAME\"$nobold to define $bold\"$domain_name\"$nobold"

clone_domain "$FAAS_BASE_DOMAIN_NAME" "$FAAS_BASE_DOMAIN_IMAGE" "$domain_name" "$domain_image"

print_info "You can now run the VM and get a console to it with:"
print_info "\t${bold}./run-vm.sh $domain_name$nobold"
print_info "Log in as ${bold}root$nobold."

print_subtitle "setup $domain_name as a FaaS node"

print_warn "Start this machine with $bold\`\"$SCRIPTDIR/run-vm.sh\" $domain_name\`$nobold to setup the Kubernetes cluster."
printf "* if this is the first node: ${bold}/mnt/host-sharing/make-kubernetes-cluster.sh$nobold\n"
printf "* on other nodes: ${bold}/mnt/host-sharing/join-cluster.sh$nobold\n"

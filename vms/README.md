# Virtual machines for FaaS stresstest

## Using local libvirt and virtiofsd versions

Here, local versions of libvirt and virtiofsd are used (you do not need to start them manually).
**Do not directly execute libvirt commands:** use provided scripts instead.

If you need to run `virsh`, first `source env`, and you will gain access to the command `libvirt_run` to run commands such as `virsh` as follows:

```shell
# Only needed once per shell.
source env
libvirt_run virsh ...
```

## Scripts

Run them in order (no need to `source env` first, but that doesn't hurt):

1. `setup.sh`: setup local versions of libvirt and virtiofsd
2. `define-vm_debian-12.sh`: define a vanilla Debian 12 VM, used as a base for other VMs
3. `define-vm_faas0.sh`: define the base VM for nodes of an OpenWhisk cluster
4. `define-vm_new-faas-node.sh`: define a new VM to serve as a node of an OpenWhisk cluster
   * run as many times as you need nodes in your cluster
5. `run-vm.sh VM`: run a VM

## VM images

They use qcow2 in [backing chains](https://www.libvirt.org/kbase/backing_chains.html): a VM image is based on a previous one, like a layer;
the previous base image is unmodified when running the VM.

* `debian-12-nocloud-amd64.qcow2`: base Debian 12 image, downloaded once, never modified
* `debian-12-nocloud.qcow2`: base Debian 12 image, customized
  * based on `debian-12-nocloud-amd64.qcow2`
* `faas0.qcow2`: base FaaS node, where Kubernetes gets installed during the first boot
  * based on `debian-12-nocloud.qcow2`
  * to be booted only once to install Kubernetes
* `faas1.qcow2`, `faas2.qcow2`, etc.: FaaS nodes
  * each based on `faas0.qcow2`

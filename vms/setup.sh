#! /bin/bash

DEPENDENCIES=(git pyenv meson ninja cargo virt-resize virt-sysprep virsh)

SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"
source "$SCRIPTDIR/env"
source "$SCRIPTDIR/fancyterm.sh"

function ensure_submodule() {
    if git submodule status "$1" | grep --quiet '^-'; then
        print_info "updating $bold$(basename "$1")$nobold at $bold\"$1\"$nobold"
        git submodule update --init --recursive "$1" ||
            die "failed updating Git submodule at $bold\"$1\"$nobold"
    else
        print_warn "using existing submodule for $bold$(basename "$1")$nobold"
    fi
}

for dep in "${DEPENDENCIES[@]}"; do
    if ! which "$dep" > /dev/null; then
        die "missing dependency: $bold$dep$nobold"
    fi
done

print_info "found all dependencies ($bold${DEPENDENCIES[*]}$nobold)"

if ! ip link | grep --quiet "$LIBVIRT_BRIDGE"; then
    die "bridge interface $bold$LIBVIRT_BRIDGE$nobold not found;\nmaybe start it with $bold\`sudo virsh net-start default\`$nobold?"
fi

print_info "found bridge interface $bold$LIBVIRT_BRIDGE$nobold"

if ! [ -x "$LIBVIRTD_EXE" ]; then
    print_info "building libvirtd"

    ensure_submodule "$LIBVIRT_DP"

    pushd "$LIBVIRT_DP" ||
        die "failed changing directory to libvirt repository at $bold\"$LIBVIRT_DP\"$nobold"

    git checkout v10.4.0

    # We are in a pipenv Python virtual environment, that uses pyenv to manage the local Python version: make sure the
    # latter is installed.
    pyenv install --skip-existing ||
        die "failed installing local Python version"

    meson setup build -Dsystem=true -Ddriver_qemu=enabled ||
        die "failed preparing build of libvirt"
    ninja -C build ||
        die "failed building libvirt"

    popd ||
        die "failed returning to starting directory"
else
    print_warn "using existing libvirt at $bold\"$LIBVIRTD_EXE\"$nobold"
fi

if ! [ -x "$VIRTIOFSD_EXE" ]; then
    print_info "building virtiofsd"

    ensure_submodule "$VIRTIOFSD_DP"

    pushd "$VIRTIOFSD_DP" ||
        die "failed changing directory to virtiofsd repository at $bold\"$VIRTIOFSD_DP\"$nobold"

    git checkout v1.11.0

    cargo build --release ||
        die "failed building virtiofsd"

    popd ||
        die "failed returning to starting directory"
else
    print_warn "using existing virtiofsd at $bold\"$VIRTIOFSD_EXE\"$nobold"
fi

print_title "Using local libvirt and virtiofsd"

printf "To run virsh and other commands:\n"
printf "1. source the environment configuration in your current shell with $bold\`source \"%s\"\`$nobold\n" "$SCRIPTDIR/env"
printf "2. execute with $bold\`libvirt_run\`$nobold, e.g.: $bold\`libvirt_run virsh ...\`$nobold\n"
printf "Sourcing $bold\`%s\`$nobold will also enable kubectl to connect to the Kubernetes cluster when it is ready\n" "$SCRIPTDIR/env"

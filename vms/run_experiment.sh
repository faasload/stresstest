#! /bin/bash

SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"
source "$SCRIPTDIR/env"
source "$SCRIPTDIR/fancyterm.sh"

expe="$1"
shift

cd "$SCRIPTDIR/.." && pipenv run python -m experiment."$expe".run "$@"

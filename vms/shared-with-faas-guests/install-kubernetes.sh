#! /bin/bash

set -e

# Install prerequisites to add PPAs.
apt-get update
apt-get install --assume-yes ca-certificates curl wget apt-transport-https ca-certificates curl gpg

install -m 0755 -d /etc/apt/keyrings

# Add Docker's official GPG key.
curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc
# Add Kubernetes's official GPG key.
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.30/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

# Add Docker's repository to Apt sources.
echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" >/etc/apt/sources.list.d/docker.list
# Add Kubernetes's repository to Apt sources.
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.30/deb/ /' >/etc/apt/sources.list.d/kubernetes.list

# Fetch updates from new repositories and install Docker and Kubernetes.
apt-get update
apt-get install --assume-yes docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin kubelet kubeadm kubectl

# Install cri-dockerd.
wget https://github.com/Mirantis/cri-dockerd/releases/download/v0.3.14/cri-dockerd_0.3.14.3-0.debian-bookworm_amd64.deb --output-document /tmp/cri-dockerd.deb
dpkg -i /tmp/cri-dockerd.deb

# "Start" Kubernetes: enable the kubelet.
systemctl enable --now kubelet

# Pull the Kubernetes images beforehand.
kubeadm config images pull --cri-socket unix:///var/run/cri-dockerd.sock

# Prepare environment in future nodes when they will have joined the cluster.
echo "export KUBECONFIG=/mnt/host-sharing/admin.conf" >>"/root/.profile"

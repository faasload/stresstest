#! /bin/bash

SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"
SCRIPT_FP="$(realpath "$0")"
SCRIPT_UID="$(ls --numeric-uid-gid "$SCRIPT_FP" | awk '{ print $3 }')"
SCRIPT_GID="$(ls --numeric-uid-gid "$SCRIPT_FP" | awk '{ print $4 }')"

JOINCLUSTER_SCRIPT_FP="$SCRIPTDIR/join-cluster.sh"

kubeadm init --pod-network-cidr=10.244.0.0/16 --cri-socket unix:///var/run/cri-dockerd.sock

cp /etc/kubernetes/admin.conf "$SCRIPTDIR"
chown "$SCRIPT_UID:$SCRIPT_GID" "$SCRIPTDIR/admin.conf"

export KUBECONFIG=/etc/kubernetes/admin.conf
kubectl apply -f https://github.com/flannel-io/flannel/releases/latest/download/kube-flannel.yml

host_ip="$(ip -family inet addr show enp1s0 | awk '/inet / {print $2}' | cut -d"/" -f1)"
token="$(kubeadm token list -o jsonpath='{.token}')"
cert_hash="$(openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null |
    openssl dgst -sha256 -hex | sed 's/^.* //')"
echo "#! /bin/bash" >"$JOINCLUSTER_SCRIPT_FP"
printf "kubeadm join %s:6443 --token %s --discovery-token-ca-cert-hash sha256:%s --cri-socket unix:///var/run/cri-dockerd.sock" "$host_ip" "$token" "$cert_hash" >>"$JOINCLUSTER_SCRIPT_FP"
chmod u+x "$JOINCLUSTER_SCRIPT_FP"

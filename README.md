# FaaSLoad: stresstest evaluation

## Contents

* [`vms`](vms/): scripts and resources to build VMs used as nodes for the FaaS cluster

## Setup

See the initial setup for FaaS nodes in [`vms`](vms/).

Get the git submodules:

```shell
git submodule update --init
```

Deploy OpenWhisk:

```shell
experiment/platform-configurations/openwhisk/kubernetes/deploy.sh
```

Install the embedded FaaSLoad's virtual environment:

```shell
cd experiment/faasload && pipenv install
```

Finish with `experiment/setup.sh`.

## Troubleshooting

### Errors related to `~/.kube/config`

Source `vms/env`.

### Reset after an experiment run

Need to unload the OpenWhisk assets **using the configuration file `~/.config/faasload/loader.yml`** used by the experiment (to point to the correct user authentication keys).

**Note:** if FaaSLoad started running at all, you must also drop the database tables before unloading the assets.
Or course, this means losing all data in it, so if you do want to keep FaaSLoad's results themselves, dump the DB beforehand.
To drop all tables:

```shell
# THIS WILL ERASE ALL DATA STORED BY FAASLOAD (not by the stresstest experiments)
docker exec -it faasload-db psql --user faasload faasload -c "drop table dockeroperations, resources, results, runs, parameters;"
```

So, before running any other experiment:

```shell
experiment/faasload.sh scripts/load-wsk-assets.py --unload ~/FaaSLoad/actions/manifest-dummy.yaml --injector INJECTION_TRACES
```

If you did run another experiment, the authentication tokens are all wrong or missing:

0. remove any existing user authentication tokens in the directory pointed to by FaaSLoad's loader configuration
1. run the above command with `--revert` instead of `--unload` (yes, reusing the same injection traces), this will:
   1. try to load assets, creating new user authentication tokens;
   2. fail because assets already exist;
   3. unload them thanks to the `--revert`

_**TODO**_: outdated README below

## Usage

### Monitor: missed measurements

Stress test the monitor about missed measurements.

1. build the stress test Docker image
```shell
cd stresstest-monitor-sleep`
docker build . --tag faasload/stresstest-sleep
```
2. run the stress test script `./monitor-missed-measurements.sh N DELAY`
   * `N`: run up to this number of stress test containers
   * `DELAY`: time to wait (seconds) before running the next container
   * other settings may be changed as constants in the script, including the tested monitoring resolutions and the image of test containers
3. results are in "results/monitor-missed-measurements"
   * one folder per value of monitoring resolution
   * each folder contains the logs of the monitor
4. run the results parser script: `scripts/parse-monitor-missed-measurements.py`
   * parsed results are kept in binary format with each source result (i.e., each monitor log)
   * the binary format is the pickled representation of the data used by the Jupyter notebook
5. run the Jupyter notebook, it reads and presents the parsed results

### Loader: delayed invocations

1. clone FaaSLoad's repository of sample actions and build them:
```shell
git clone https://gitlab.com/faasload/actions.git ..
cd ../actions && make
```
2. run the stress test script `./loader-bursty.sh N PERIOD`
   * `N`: number of burst periods in the test
   * `PERIOD`: duration of one complete burst period (seconds)
     * a burst period is a complete cycle of "low", "ramp up", "high", "ramp down"
   * other settings may be changed as constants in the script, including other customization points of the burst periods and the invoked action
3. results are in "results/loader-bursty"
   * it contains the logs of the loader
4. _currently, our experiments show that OpenWhisk is the bottleneck on our server, so there are no results to parse nor present in the Jupyter notebook_

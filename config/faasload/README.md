This folder gets symlinked to `$HOME/.config/faasload` by `experiment/setup.sh` to provide FaaSLoad's configuration specific to an experiment.

Each experiment has templates for FaaSLoad's configuration files;
their run scripts fill in their templates to create the `loader.yaml` for them.

In some way, experiments compete for FaasLoad's `loader.yaml`

#! /bin/env python3

import logging
import shlex
import subprocess as sp
import sys
import tomllib as toml
from importlib import import_module
from os import path

import enoslib as en
from enoslib import actions as enos_actions
from enoslib.errors import EnosFailedHostsError, EnosUnreachableHostsError

JOB_NAME = "faasload-stresstest_faasload"
# Count for a latency of 10min between starting the FaaSLoad job, and starting the K8S cluster job.
# TODO start the K8S cluster job first
# better TODO: start both jobs in parallel and synchronize
# better better TODO: start one job and manage to have kube5k use its nodes (except the one running FaaSLoad)
FAASLOAD_JOB_WALLTIME = "02:00:00"

EXPERIMENT_GITURL = "https://gitlab.com/faasload/stresstest.git"
ACTIONS_GITURL = "https://gitlab.com/faasload/actions.git"

# On both
K8S_ADMIN_CONF_FN = "admin_faasload-stresstest_k8s.conf"
ACTION_GO_DUMMY_REL_FP = path.join("build", "go", "none", "dummy.zip")

# On the local host.
SCRIPT_DP = path.normpath(path.abspath(path.realpath(path.dirname(__file__))))

KUBE5K_CONF_FN = "kube5k_cluster-conf.yaml"
OPENWHISK_CLUSTER_TMPL_FN = "openwhisk-cluster.tmpl.yaml"

EXPERIMENT_CONF_DP_HOST = path.join(SCRIPT_DP, "..", "config/")
KUBE5K_CONF_FP = path.join(SCRIPT_DP, KUBE5K_CONF_FN)
OPENWHISK_CLUSTER_TMPL_FP = path.join(SCRIPT_DP, OPENWHISK_CLUSTER_TMPL_FN)

# On the remote.
ACTIONS_DN = "actions"
EXPERIMENT_DN = "stresstest"
G5K_DN = "g5k"

ACTION_GO_DUMMY_FP_REMOTE = path.join(ACTIONS_DN, ACTION_GO_DUMMY_REL_FP)
EXPERIMENT_CONF_DP_REMOTE = path.join(EXPERIMENT_DN, "config/")
FAASLOAD_DP = path.join(EXPERIMENT_DN, "experiment", "faasload")
G5K_DP = path.join(EXPERIMENT_DN, G5K_DN)
KUBECONFIG_FP = path.join(G5K_DP, K8S_ADMIN_CONF_FN)
K8S_DEPLOY_SCRIPT_FN = "deploy.sh"
K8S_DEPLOY_SCRIPT_FP = path.join(
    EXPERIMENT_DN, "experiment", "platform-configurations", "openwhisk", "kubernetes", K8S_DEPLOY_SCRIPT_FN
)


def main():
    logger = logging.getLogger("deploy")

    sys.path.append(path.abspath(path.join(SCRIPT_DP, "..")))
    try:
        experiment_module_name = sys.argv[1]
        experiment_name = import_module("experiment." + experiment_module_name + ".lib").EXPERIMENT_NAME
    except IndexError:
        logger.error("Expected 1 argument: the experiment Python module name under experiment")
        sys.exit(2)
    except ImportError:
        logger.exception("Failed importing experiment module %s", experiment_module_name)
        sys.exit(2)

    expe_conf_fp = path.join(EXPERIMENT_CONF_DP_HOST, experiment_name, "config.toml")
    with open(expe_conf_fp, "rb") as conf_f:
        ACTION_GO_DUMMY_FP_HOST = path.join(
            path.dirname(path.normpath(path.join(SCRIPT_DP, "..", toml.load(conf_f)["action"]["manifest_fp"]))),
            ACTION_GO_DUMMY_REL_FP,
        )

    en.check()

    logger.info("checked EnOSlib")

    en_conf = en.G5kConf.from_settings(
        job_name=JOB_NAME,
        job_type=["deploy"],
        env_name="debian12-nfs",
        walltime=FAASLOAD_JOB_WALLTIME,
    ).add_machine(
        roles=["faasload"],
        cluster="gros",
        nodes=1,
    )

    en_provider = en.G5k(en_conf)
    # en_provider.set_reservation(1721934000)

    logger.info("validated EnOSlib configuration")
    logger.debug("EnOSlib configuration:\n%s", en_conf)

    logger.info("reserving and preparing resources")

    roles, networks = en_provider.init()
    en.wait_for(roles)

    logger.info("resources ready")
    logger.info("FaaSLoad will run on node %s", roles["faasload"][0])

    # TODO I essentially wrote Ansible playbooks in Python (and badly on top of that)... Do it better!
    with enos_actions(roles=roles["faasload"]) as actions:
        actions.git(
            task_name="Clone experiment repository",
            repo=EXPERIMENT_GITURL,
            dest=EXPERIMENT_DN,
            accept_hostkey=True,
            recursive=True,
            force=True,
        )
        actions.copy(
            task_name="Copy experiment configuration",
            src=EXPERIMENT_CONF_DP_HOST,
            dest=EXPERIMENT_CONF_DP_REMOTE,
        )
        # Fetch the repository of actions; but due to Docker being impractical on G5K (pull rate limits, nothing is
        # otherwise present on G5K's repository cache?), actions cannot be built there, so copy from the host the needed
        # built actions where they are expected.
        actions.git(
            task_name="Clone actions repository",
            repo=ACTIONS_GITURL,
            dest=ACTIONS_DN,
            accept_hostkey=True,
            force=True,
        )
        actions.file(
            task_name="Prepare directory for built Go dummy action",
            path=path.dirname(ACTION_GO_DUMMY_FP_REMOTE),
            state="directory",
        )
        actions.copy(
            task_name="Copy built Go dummy action",
            src=ACTION_GO_DUMMY_FP_HOST,
            dest=ACTION_GO_DUMMY_FP_REMOTE,
        )

    logging.info("cloned experiment repository")

    if path.exists(K8S_ADMIN_CONF_FN):
        logging.warning("found `%s`: assuming Kubernetes cluster is ready", K8S_ADMIN_CONF_FN)
    else:
        logging.info("creating Kubernetes cluster with kube5k")

        try:
            sp.run(
                ["kube5k", "deploy", "--conf", KUBE5K_CONF_FP],
                stdout=sys.stdout,
                stderr=sys.stderr,
                check=True,
            )
        except sp.CalledProcessError:
            logger.exception("failed creating Kubernetes cluster with kube5k")
            sys.exit(1)

        logging.info("Kubernetes cluster ready")

    with enos_actions(roles=roles["faasload"]) as actions:
        actions.shell(
            task_name="Installing Helm if needed",
            cmd="curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash",
            creates="/usr/local/bin/helm",
        )
        actions.shell(
            task_name="Installing kubectl if needed",
            cmd=(
                'curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && '
                "install -m 0755 kubectl /usr/local/bin/kubectl"
            ),
            creates="/usr/local/bin/kubectl",
        )

        actions.copy(task_name="Copy cluster config file for kubectl", src=K8S_ADMIN_CONF_FN, dest=G5K_DP)

        actions.command(
            task_name="Setup experiment environment",
            cmd="experiment/setup.sh",
            chdir=EXPERIMENT_DN,
            environment=dict(KUBECONFIG=path.relpath(KUBECONFIG_FP, start=EXPERIMENT_DN)),
        )

        actions.copy(
            task_name="Copy OpenWhisk cluster template configuration file",
            src=OPENWHISK_CLUSTER_TMPL_FP,
            dest=path.join(path.dirname(K8S_DEPLOY_SCRIPT_FP), "mycluster.tmpl.yaml"),
        )
        actions.file(
            task_name="Remove previously generated OpenWhisk cluster configuration file if present",
            path=path.join(path.dirname(K8S_DEPLOY_SCRIPT_FP), "mycluster.gen.yaml"),
            state="absent",
        )

        actions.command(
            task_name="Deploying OpenWhisk for FaaSLoad",
            cmd=f"bash {K8S_DEPLOY_SCRIPT_FN}",
            chdir=path.dirname(K8S_DEPLOY_SCRIPT_FP),
            environment=dict(KUBECONFIG=path.relpath(KUBECONFIG_FP, path.dirname(K8S_DEPLOY_SCRIPT_FP))),
        )

        actions.apt(
            task_name="Install pyenv built dependencies if needed",
            name=["make", "libssl-dev"],
            state="present",
            update_cache=True,
        )
        actions.shell(task_name="Install pyenv if needed", cmd="curl https://pyenv.run | bash", creates="/root/.pyenv")
        actions.shell(
            task_name="Setup Shell environment for pyenv",
            cmd="""
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.profile
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.profile
echo 'eval "$(pyenv init -)"' >> ~/.profile
""",
        )
        actions.apt(
            task_name="Install perf if needed",
            name=["linux-perf"],
            state="present",
            update_cache=True,
        )

    with enos_actions(roles=roles["faasload"], gather_facts=True) as actions:
        # Install versions to create shims (including for pip, which is used below).
        actions.shell(
            task_name="Install Python 3.10.4 with pyenv if needed",
            cmd="source ~/.profile && pyenv install 3.10.4",
            creates="{{ ansible_env.HOME }}/.pyenv/versions/3.10.4",
            executable="/bin/bash",
        )
        actions.shell(
            task_name="Install Python 3.12.4 with pyenv if needed",
            cmd="source ~/.profile && pyenv install 3.12.4",
            creates="{{ ansible_env.HOME }}/.pyenv/versions/3.12.4",
            executable="/bin/bash",
        )

    with enos_actions(roles=roles["faasload"], gather_facts=True) as actions:
        actions.shell(
            task_name="Prepare installing pipenv environment for FaaSLoad",
            cmd="source ~/.profile && pip install --upgrade pip pipenv",
            chdir=FAASLOAD_DP,
            executable="/bin/bash",
        )
        actions.shell(
            task_name="Install pipenv environment for FaaSLoad",
            cmd="source ~/.profile && pipenv install",
            chdir=FAASLOAD_DP,
            executable="/bin/bash",
        )
        actions.shell(
            task_name="Prepare installing pipenv environment for the experiment",
            cmd="source ~/.profile && pip install --upgrade pip pipenv",
            chdir=EXPERIMENT_DN,
            executable="/bin/bash",
        )
        actions.shell(
            task_name="Install pipenv environment for the experiment",
            cmd="source ~/.profile && pipenv install --categories experiment",
            chdir=EXPERIMENT_DN,
            executable="/bin/bash",
        )

        actions.apt_key(
            task_name="Get GPG key to install TimeScaleDB",
            url="https://packagecloud.io/timescale/timescaledb/gpgkey",
            keyring="/etc/apt/trusted.gpg.d/timescaledb.gpg",
            state="present",
        )
        actions.apt_repository(
            task_name="Add TimeScaleDB repository",
            repo="deb https://packagecloud.io/timescale/timescaledb/debian/ {{ ansible_distribution_release }} main",
            filename="timescaledb",
            state="present",
        )
        actions.apt(
            task_name="Install PostgreSQL, TimescaleDB and dependencies",
            name=["python3-psycopg2", "postgresql", "libpq-dev", "timescaledb-2-postgresql-15", "postgresql-client"],
            state="present",
            update_cache=True,
        )
        actions.shell(task_name="Tune PostgreSQL for TimeScaleDB", cmd="yes | timescaledb-tune")
        actions.systemd_service(task_name="Restart PostgreSQL", name="postgresql", state="restarted")

    with enos_actions(roles=roles["faasload"], run_as="postgres") as actions:
        actions.postgresql_user(name="faasload", password="faasload", state="present")
        actions.postgresql_db(name="faasload", owner="faasload", state="present")
        actions.postgresql_ext(name="timescaledb", db="faasload", state="present")

    logger.info("running the experiment!")

    try:
        en.run_command(
            f"source ~/.profile && pipenv run python -m experiment.{experiment_module_name}.run "
            + shlex.join(sys.argv[2:]),
            roles=roles,
            executable="/bin/bash",
            chdir=EXPERIMENT_DN,
            environment=dict(KUBECONFIG=path.relpath(KUBECONFIG_FP, start=EXPERIMENT_DN)),
        )
    except EnosFailedHostsError:
        logger.error("failed running experiment command")
    except EnosUnreachableHostsError:
        logger.error("failed running experiment command: host is unreachable")
        sys.exit(1)

    logger.info("fetching experiment output data")

    with enos_actions(roles=roles["faasload"], run_as="postgres") as actions:
        actions.postgresql_db(
            task_name="Dump FaaSLoad DB",
            name="faasload",
            owner="faasload",
            target="/tmp/faasload.sql",
            state="dump",
        )
    with enos_actions(
        roles=roles["faasload"],
        extra_vars=dict(
            result_faasload_db=path.join(EXPERIMENT_DN, "results", experiment_name, "latest", "faasload.sql")
        ),
    ) as actions:
        actions.command(
            task_name="Move FaaSLoad DB dump to results directory",
            cmd="mv /tmp/faasload.sql {{ result_faasload_db }}",
        )

    # There is no recursive "fetch" actions with Ansible: use system's rsync.
    sp.run(
        [
            "rsync",
            "-arv",
            f"root@{roles['faasload'][0].address}:{EXPERIMENT_DN}/results",
            path.abspath(path.join(SCRIPT_DP, "..")),
        ]
    )
    sp.run(
        [
            "rsync",
            "-arv",
            f"root@{roles['faasload'][0].address}:{EXPERIMENT_DN}/logs",
            path.abspath(path.join(SCRIPT_DP, "..")),
        ]
    )

    logger.info("fetched experiment output data")

    # TODO destroy resources
    # also destroy K8S cluster
    # but let's try to schedule more experiment using it!
    # en_provider.destroy()


if __name__ == "__main__":
    en.init_logging(level=logging.INFO)

    main()

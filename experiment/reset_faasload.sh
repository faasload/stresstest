#! /bin/bash

SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"

if [ $# -lt 1 ]; then
    echo "Usage: $0 LAST_EXPERIMENT_NAME"
    echo "LAST_EXPERIMENT_NAME is the name of the last experiment: loader_burst, loader_concurrent, etc."
    exit 2
fi >&2
expe_name="$1"

case "$expe_name" in
azure) injection_traces="$(eval echo "$(grep traces_dp "$SCRIPTDIR/../results/$expe_name/latest/parameters.toml" | cut -d" " -f3)")" ;;
*) injection_traces="$SCRIPTDIR/../results/$expe_name/latest/injection-traces" ;;
esac

if which psql >/dev/null 2>&1; then
    PGPASSWORD=faasload psql --host localhost --user faasload faasload --command "drop table dockeroperations, resources, results, runs, parameters;"
else
    PGPASSWORD=faasload docker exec --interactive --tty faasload-db psql --user faasload faasload --command "drop table dockeroperations, resources, results, runs, parameters;"
fi

"$SCRIPTDIR"/faasload.sh scripts/load-wsk-assets.py --unload \
    "$(eval echo "$(grep manifest_fp "$SCRIPTDIR/../results/$expe_name/latest/parameters.toml" | cut -d" " -f3)")" \
    --injector "$injection_traces"

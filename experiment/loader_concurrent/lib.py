from dataclasses import asdict, dataclass
from enum import Enum
from os import path
from typing import Any, NamedTuple

import tomlkit as toml
from tomlkit.items import Null as TOMLNull

EXPERIMENT_NAME = path.basename(path.dirname(__file__))


class ExperimentRunStatus(Enum):
    Success = "success"
    FaaSLoadRunFailure = "faasload-run-failure"
    ExecutionFailure = "execution-failure"
    UserInterrupt = "user-interrupt"

    def __str__(self) -> str:
        return self.value


class InjectionTimingError(NamedTuple):
    timestamp: float
    error: float


class PerfEvent(NamedTuple):
    timestamp: float
    value: float | None


InjectionTimingErrors = dict[str, list[InjectionTimingError]]
PerfEvents = dict[str, list[PerfEvent]]


@dataclass
class PerfResult:
    events: PerfEvents
    multiplexing_warning: bool

    @classmethod
    def from_toml(cls, table) -> "PerfResult":
        return cls(
            events={
                event_name: [PerfEvent(timestamp=event[0], value=event[1]) for event in events]
                for event_name, events in table["events"].unwrap().items()
            },
            multiplexing_warning=table["multiplexing_warning"],
        )


@dataclass
class ExperimentRunResult:
    status: ExperimentRunStatus
    injection_timing_errors: InjectionTimingErrors | None
    perf: PerfResult | None

    def to_toml(self) -> dict[str, Any]:
        self_d = asdict(self)
        self_d["status"] = self.status.value
        if self.injection_timing_errors is None:
            del self_d["injection_timing_errors"]
        if self.perf is None:
            del self_d["perf"]

        return self_d

    @classmethod
    def from_toml(cls, table) -> "ExperimentRunResult":
        return cls(
            status=ExperimentRunStatus(table["status"]),
            injection_timing_errors=(
                {
                    user: [InjectionTimingError(timestamp=error[0], error=error[1]) for error in errors]
                    for user, errors in table["injection_timing_errors"].unwrap().items()
                }
                if table.get("injection_timing_errors", None)
                else {}
            ),
            perf=PerfResult.from_toml(table["perf"]) if table.get("perf", None) else None,
        )


class ExperimentStatus(Enum):
    """Status of the experiment."""

    Success = "success"
    LoadWskAssetsFailure = "load-wsk-assets-failure"
    RunFailure = "run-failure"
    UserInterrupt = "user-interrupt"

    def __str__(self) -> str:
        return self.value


@dataclass
class ExperimentResult:
    """Result of the concurrent experiment variant."""

    nb_runs: int | None
    runs: list[ExperimentRunResult] | None
    status: ExperimentStatus = ExperimentStatus.Success

    def __str__(self) -> str:
        if self.status is ExperimentStatus.LoadWskAssetsFailure or self.nb_runs is None or self.runs is None:
            return f"ExperimentResult(status={self.status.value})"
        else:
            return f"ExperimentResult(status={self.status.value}, runs={self.nb_runs})"

    def to_toml(self) -> dict[str, Any]:
        self_d = asdict(self)
        if self.nb_runs is None:
            del self_d["nb_runs"]
        if self.runs is None:
            del self_d["runs"]
        else:
            self_d["runs"] = [run.to_toml() for run in self.runs]
        self_d["status"] = self.status.value

        return self_d

    @classmethod
    def from_toml(cls, table) -> "ExperimentResult":
        return cls(
            nb_runs=table.get("nb_runs", TOMLNull()).unwrap(),
            runs=[ExperimentRunResult.from_toml(run) for run in table.get("runs", toml.array())],
            status=ExperimentStatus(table["status"]),
        )

import re
import sys
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from collections import defaultdict
from datetime import datetime, time, timedelta
from math import ceil
from os import PathLike, makedirs, path
from subprocess import CalledProcessError
from subprocess import run as sp_run
from textwrap import dedent
from typing import Any, TextIO

import tomlkit
import yaml

from experiment import (
    GlobalConfiguration,
    configure_logging,
    get_experiment_logger,
    load_global_config,
    make_log_dp,
    make_result_dp,
    write_experiment_parameters,
    write_global_config,
)
from experiment.trace_builder import make_injection_traces

from .config import ExperimentConfiguration, ExperimentParameters, load_config
from .lib import (
    EXPERIMENT_NAME,
    ExperimentResult,
    ExperimentRunResult,
    ExperimentRunStatus,
    ExperimentStatus,
    InjectionTimingError,
    InjectionTimingErrors,
    PerfEvent,
    PerfEvents,
    PerfResult,
)
from .spec_maker import ConcurrentSpecMaker


def parse_args(config: ExperimentConfiguration) -> ExperimentParameters:
    parser = ArgumentParser(
        formatter_class=RawDescriptionHelpFormatter,
        description=sys.modules[__name__].__doc__,
        epilog=dedent(
            f"""
            Experiment configuration: {config}.
            """
        ),
    )

    parser.add_argument("period", type=float, help="period (seconds) before injecting more functions")
    parser.add_argument("new_concurrents", metavar="new", type=int, help="number of new functions added every period")
    parser.add_argument("end_concurrents", metavar="end", type=int, help="final number of concurrent functions")
    parser.add_argument(
        "start_concurrents",
        metavar="start",
        type=int,
        nargs="?",
        default=1,
        help="initial number of concurrent functions",
    )

    return ExperimentParameters(concurrent=config, **vars(parser.parse_args()))


def make_loader_config(values: dict[str, Any], copy_fp: str | PathLike | None = None, config_dp: str = "config"):
    with open(path.join(config_dp, EXPERIMENT_NAME, "loader.tmpl.yml"), "r") as tmpl_f:
        tmpl = tmpl_f.read()

    with open(path.join(config_dp, "faasload", "loader.yml"), "w") as config_f:
        config_f.write(tmpl.format(**values))

    if copy_fp:
        with open(copy_fp, "w") as copy_f:
            copy_f.write(tmpl.format(**values))


TIMING_ERROR_LINE_RE = re.compile(
    r"^\[(.+)]\[WARNING]\[injector]\[(.+?)/.*] injection of trace point #.+ (.+) by (.+) \(greater than .*$"
)


def parse_injection_timing_errors(lines: list[str], injection_start: datetime | None) -> InjectionTimingErrors:
    errors = defaultdict(list)

    if injection_start is None:
        return {}

    for line in lines:
        matches = re.match(TIMING_ERROR_LINE_RE, line)
        if matches:
            # The str repr. of a timedelta only outputs one digit at the hours place, but the ISO format needs 2...
            error_time = time.fromisoformat("0" + matches[4])
            delta = timedelta(
                hours=error_time.hour,
                minutes=error_time.minute,
                seconds=error_time.second,
                microseconds=error_time.microsecond,
            )
            errors[matches[2]].append(
                InjectionTimingError(
                    timestamp=(datetime.strptime(matches[1], "%Y-%m-%d %H:%M:%S") - injection_start).total_seconds(),
                    error=(1 if matches[3] == "late" else -1) * delta.total_seconds(),
                )
            )

    return errors


def extract_injection_timing_info(run_stderr: TextIO) -> tuple[datetime, list[str]]:
    injection_start = None
    injection_timing_errors_lines: list[str] = []

    run_stderr.seek(0)
    for line in run_stderr:
        if "injection of trace point" in line:
            injection_timing_errors_lines.append(line)
        elif "[injection] starting" in line:
            match = re.match(r"^\[(.+)]\[INFO].*$", line)
            if match:
                injection_start = datetime.strptime(match[1], "%Y-%m-%d %H:%M:%S")

    if injection_start is None:
        raise ValueError("did not find start of injection")

    return injection_start, injection_timing_errors_lines


def parse_perf_output(perf_log_fp: str) -> PerfResult:
    events: PerfEvents = defaultdict(list)
    multiplexing_warning = False

    with open(perf_log_fp, "r") as perf_output:
        for line in perf_output:
            line = line.rstrip()
            if not line or line[0] == "#":
                continue

            parsed_line = line.split(",")

            if float(parsed_line[5]) < 100.00:
                multiplexing_warning = True

            # Try to get the metric computed by perf, otherwise try to get the event value.
            # This is expected to be consistent over samples of the same event: if there is no metric for a sample while
            # it is usually calculated, then it means that the event was not counter at all for this sample.
            if not parsed_line[6]:
                if parsed_line[1] == "<not counted>":
                    value = float("nan")
                else:
                    # Let it raise in case of bad value, but this is not expected.
                    value = float(parsed_line[1])
            else:
                # Let it raise in case of bad value, but this is not expected.
                value = float(parsed_line[6])

            events[parsed_line[3].split(":")[0]].append(
                PerfEvent(
                    timestamp=float(parsed_line[0]),
                    value=value,
                )
            )

    return PerfResult(events=events, multiplexing_warning=multiplexing_warning)


class Experiment:
    def __init__(self, global_config: GlobalConfiguration):
        expe_datetime = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

        self.params = parse_args(load_config())
        self.faasload_config = global_config.faasload

        directory_name = expe_datetime
        self.result_dp = make_result_dp(global_config, EXPERIMENT_NAME, directory_name)
        self.log_dp = make_log_dp(global_config, EXPERIMENT_NAME, directory_name)
        self.result_fp = path.join(self.result_dp, global_config.filenames.experiment_result)

        self.logger = get_experiment_logger(global_config, EXPERIMENT_NAME, self.log_dp)

        self.rngseed = global_config.rngseed

        self.nb_runs = 0
        self.samples: list[ExperimentRunResult] = list()

    def write_configuration_and_parameters(self, global_config: GlobalConfiguration):
        self.logger.debug("global config: %s", global_config)

        global_config_fp = write_global_config(global_config, self.result_dp)
        self.logger.info("wrote global configuration to `%s`", global_config_fp)

        self.logger.debug("experiment parameters: %s", self.params)

        params_fp = write_experiment_parameters(global_config, self.result_dp, self.params)
        self.logger.info("wrote experiment parameters to `%s`", params_fp)

    def _make_user_spec(self, spec_fp: str | PathLike) -> dict:
        maker = ConcurrentSpecMaker(
            action=self.params.concurrent.action.name,
            invocation_rate=self.params.concurrent.invocation_rate,
            period=self.params.period,
            new_concurrents=self.params.new_concurrents,
            end_concurrents=self.params.end_concurrents,
            start_concurrents=self.params.start_concurrents,
        )
        spec = maker.make()

        with open(spec_fp, "w") as spec_f:
            spec_f.write(f"# {repr(maker)}\n")
            yaml.dump(spec, spec_f)

        return spec

    def _run_once(
        self, run_log_fp: str, perf_events: list[str] | None = None, perf_log_fp: str | None = None
    ) -> ExperimentRunResult:
        """Run the experiment once.

        To disable running FaaSLoad under perf, `perf_events` must be `None`.
        If it is the empty list, it will run under perf but without any events specified, which will run perf with its default list of events.
        """
        if perf_events is not None and not perf_log_fp:
            raise ValueError("bad file path to store perf log")

        injection_start = None
        injection_timing_errors_lines: list[str] = []

        result = lambda status: ExperimentRunResult(
            status=status,
            injection_timing_errors=parse_injection_timing_errors(injection_timing_errors_lines, injection_start),
            perf=parse_perf_output(perf_log_fp) if perf_events is not None and perf_log_fp else None,
        )

        self.logger.info("experiment run #%d", self.nb_runs)

        self.logger.info("running FaaSLoad loader, follow logs at `%s`", run_log_fp)

        faasload_loader_cmd = [
            self.faasload_config.runner_script_fp,
            "python",
            "-m",
            "faasload.loader",
        ]
        if perf_events is not None and perf_log_fp:
            faasload_loader_cmd = (
                [
                    "perf",
                    "stat",
                    "--field-separator",
                    ",",
                    "--output",
                    perf_log_fp,
                    "--interval-print",
                    "1000",
                ]
                + (["-e", ",".join(perf_events)] if perf_events else [])
                + faasload_loader_cmd
            )
        with open(run_log_fp, "w+") as run_stderr:
            try:
                sp_run(
                    faasload_loader_cmd,
                    stderr=run_stderr,
                    check=True,
                    text=True,
                )
            except OSError:
                self.logger.exception("failed running FaaSLoad loader")
                return result(ExperimentRunStatus.FaaSLoadRunFailure)
            except CalledProcessError as err:
                self.logger.error("FaaSLoad loader failed with return code %d", err.returncode)
                return result(ExperimentRunStatus.ExecutionFailure)
            except KeyboardInterrupt:
                self.logger.info("interrupted")
                injection_start, injection_timing_errors_lines = extract_injection_timing_info(run_stderr)
                return result(ExperimentRunStatus.UserInterrupt)

            self.logger.info("run success")

            self.logger.info("extracting injection timing errors from log file")

            injection_start, injection_timing_errors_lines = extract_injection_timing_info(run_stderr)

        return result(ExperimentRunStatus.Success)

    def _result(self, status: ExperimentStatus) -> ExperimentResult:
        return ExperimentResult(
            nb_runs=self.nb_runs,
            runs=self.samples,
            status=status,
        )

    def _run(self) -> ExperimentResult:
        self.logger.info("making concurrent user spec")

        spec_fp = path.join(self.result_dp, self.params.concurrent.userspec_fn)
        spec = self._make_user_spec(spec_fp)

        self.logger.info("made concurrent user spec at `%s`", spec_fp)

        self.logger.info("making injection trace")

        traces_dp = path.join(self.result_dp, self.params.concurrent.injection_traces_dn)
        makedirs(traces_dp)

        make_injection_traces(
            spec["users"],
            (ceil((self.params.end_concurrents - self.params.start_concurrents) / self.params.new_concurrents) + 1)
            * self.params.period,
            self.params.concurrent.action.manifest_fp,
            traces_dp,
            self.rngseed,
        )

        self.logger.info("wrote injection traces in `%s`", traces_dp)

        self.logger.info("making FaaSLoad loader configuration from template")

        authkeys_dp = path.join(self.result_dp, self.params.concurrent.authkeys_dn)
        loader_config_copy_fp = path.join(self.result_dp, "loader.yml")
        make_loader_config(
            dict(traces_dp=traces_dp, authkeys_dp=authkeys_dp, apihost=self.faasload_config.apihost),
            copy_fp=loader_config_copy_fp,
        )

        self.logger.info("made FaaSLoad loader configuration, with authkeys in `%s`", authkeys_dp)
        self.logger.info("this configuration is also stored at `%s`", loader_config_copy_fp)

        self.logger.info("loading OpenWhisk assets")

        loadassets_cmd = [
            self.faasload_config.runner_script_fp,
            self.faasload_config.loadwskassets_script_fp,
            self.params.concurrent.action.manifest_fp,
            "--injector",
            traces_dp,
            "--verbose",
        ]
        if self.params.concurrent.action.parameters_fp:
            loadassets_cmd.append("--param-file")
            loadassets_cmd.append(self.params.concurrent.action.parameters_fp)

        with open(path.join(self.log_dp, "load-assets.err"), "w") as loadassets_stderr:
            try:
                sp_run(
                    loadassets_cmd,
                    stderr=loadassets_stderr,
                    check=True,
                    text=True,
                )
            except OSError:
                self.logger.exception("failed running script to load OpenWhisk assets")
                return self._result(ExperimentStatus.LoadWskAssetsFailure)
            except CalledProcessError:
                self.logger.error(
                    "failed loading OpenWhisk assets, you may have to unload them manually; see logs in `%s`",
                    loadassets_stderr.name,
                )
                return self._result(ExperimentStatus.LoadWskAssetsFailure)

        self.logger.info("loaded OpenWhisk assets")

        self.logger.info("running experiment")

        try:
            self.nb_runs = 0
            for _ in range(self.params.concurrent.runs):
                self.nb_runs += 1

                run_log_fp = path.join(self.log_dp, str(self.nb_runs) + ".err")
                if self.params.concurrent.perf_events:
                    perf_log_fp = path.join(self.log_dp, f"perf_{self.nb_runs}.csv")

                try:
                    run_res = self._run_once(
                        run_log_fp,
                        self.params.concurrent.perf_events if self.params.concurrent.perf_events else None,
                        perf_log_fp if self.params.concurrent.perf_events else None,
                    )
                except Exception:
                    self.logger.exception("unexpected failure on run #%d", self.nb_runs)
                    return self._result(ExperimentStatus.RunFailure)
                else:
                    self.samples.append(run_res)

                    if run_res.status is ExperimentRunStatus.UserInterrupt:
                        self.logger.error("run #%d interrupted", self.nb_runs)
                        return self._result(ExperimentStatus.UserInterrupt)
                    if run_res.status is not ExperimentRunStatus.Success:
                        self.logger.error("run #%d failed; see logs in `%s`", self.nb_runs, run_log_fp)

            return self._result(ExperimentStatus.Success)
        except KeyboardInterrupt:
            return self._result(ExperimentStatus.UserInterrupt)

    def run(self):
        self.logger.info("running %s", self.params.concurrent.name)

        res = self._run()

        self.logger.info("experiment terminated with status %s", res.status)

        with open(self.result_fp, "x") as res_f:
            tomlkit.dump(res.to_toml(), res_f)
        self.logger.info("wrote simulation experiment results to `%s`", self.result_fp)


def main():
    global_config = load_global_config()
    configure_logging()

    expe = Experiment(global_config=global_config)
    expe.write_configuration_and_parameters(global_config)

    expe.run()


if __name__ == "__main__":
    main()

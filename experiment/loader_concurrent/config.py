import os
from dataclasses import asdict, dataclass
from typing import Any

import tomlkit

from experiment import ActionConfiguration, GlobalConfiguration

from .lib import EXPERIMENT_NAME


@dataclass
class ExperimentConfiguration:
    name: str
    runs: int

    action: ActionConfiguration
    invocation_rate: float

    userspec_fn: str
    injection_traces_dn: str
    authkeys_dn: str

    perf_events: list[str]

    def __str__(self) -> str:
        return f"{self.name}: runs: {self.runs}, action: {self.action}"

    @classmethod
    def from_toml(cls, table) -> "ExperimentConfiguration":
        return cls(
            name=table["name"].unwrap(),
            runs=table["runs"].unwrap(),
            action=ActionConfiguration.from_toml(table["action"]),
            invocation_rate=table["invocation_rate"].unwrap(),
            userspec_fn=table["userspec_fn"].unwrap(),
            injection_traces_dn=table["injection_traces_dn"].unwrap(),
            authkeys_dn=table["authkeys_dn"].unwrap(),
            perf_events=table["perf_events"].unwrap(),
        )

    def to_toml(self) -> dict[str, Any]:
        self_d = asdict(self)
        self_d["action"] = self.action.to_toml()

        return self_d


@dataclass
class ExperimentParameters:
    concurrent: ExperimentConfiguration

    period: float
    new_concurrents: int
    end_concurrents: int
    start_concurrents: int = 1

    def __str__(self) -> str:
        return (
            f"period: {self.period:.3f}s, new concurrents: {self.new_concurrents}, up to {self.end_concurrents}, "
            f"starting at {self.start_concurrents}, {self.concurrent}"
        )

    @classmethod
    def from_toml(cls, table) -> "ExperimentParameters":
        return cls(
            concurrent=ExperimentConfiguration.from_toml(table["concurrent"]),
            period=table["period"].unwrap(),
            new_concurrents=table["new_concurrents"].unwrap(),
            end_concurrents=table["end_concurrents"].unwrap(),
            start_concurrents=table["start_concurrents"].unwrap(),
        )

    def to_toml(self) -> dict[str, Any]:
        self_d = asdict(self)
        self_d["concurrent"] = self.concurrent.to_toml()

        return self_d


def load_config(config_dp: str = "config") -> ExperimentConfiguration:
    with open(os.path.join(config_dp, EXPERIMENT_NAME, "config.toml"), "r") as config_f:
        return ExperimentConfiguration.from_toml(tomlkit.load(config_f))


def load_parameters(global_config: GlobalConfiguration, results_dp: str) -> ExperimentParameters:
    with open(os.path.join(results_dp, global_config.filenames.experiment_parameters), "r") as params_f:
        return ExperimentParameters.from_toml(tomlkit.load(params_f))

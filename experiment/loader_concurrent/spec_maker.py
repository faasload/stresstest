import logging
from math import ceil

from .lib import EXPERIMENT_NAME


class ConcurrentSpecMaker:
    """Make an injection user spec for the concurrent experiment.

    The user spec makes traces start by groups of N new functions, all running under the same new user (the initial
    number of functions can also be set as Ninit).
    A new group of traces starts after the period p, untile the Nmax number of functions if reached.
    All traces invoke the same function with the same, fixed invocation rate.

    In other words, the traces will look like this (starting with 2 functions, and adding 2 functions up to 8 total):

          |    ((Nmax-Ninit)/N+1) * p     |
          |   p   |   p   |   p   |   p   |
          |-------+-------+-------+-------+--> time
    userA |  fA1  ------------------------|
          |  fA2  ------------------------|
    userB |          fB1  ----------------|
          |          fB2  ----------------|
    userC |                  fC1  --------|
          |                  fC2  --------|
    userD |                       |  fD1  |
          |                       |  fD2  |

    Note that there is a maximum duration to the user spec, and thus to the injection trace: ((Nmax-Ninit)/N+1)*p.
    In the logic of the experiment, it means that each new groupd of functions remains the newest for the time p, and the
    trace cuts after the last group has lived for the time p.

    Each group of functions is invoked under its own user, to isolate them as much as possible at the level of the FaaS platform.
    """

    def __init__(
        self,
        action: str,
        invocation_rate: float,
        period: float,
        new_concurrents: int,
        end_concurrents: int,
        start_concurrents: int = 1,
    ):
        self.action = action
        self.invocation_rate = invocation_rate
        self.period = period
        self.new = new_concurrents
        self.start = start_concurrents

        self.periods = ceil((end_concurrents - self.start) / self.new) + 1
        self.to = self.periods * self.period

        self.logger = logging.getLogger(f"experiment.{EXPERIMENT_NAME}.{self.__class__.__name__}")

    def _make_user(self, activity_from: float, nb_functions: int) -> dict:
        return {
            "memory-profile": "smart",
            "nb-inputs": {
                "image": 1,
                "video": 1,
                "audio": 1,
                "none": 1,
            },
            "actions": [
                {
                    "name": self.action,
                    "distribution": "uniform",
                    "rate": self.invocation_rate,
                    "activity-window": {"from": activity_from, "to": self.to},
                },
            ]
            * nb_functions,
        }

    def make(self) -> dict:
        self.logger.info(
            "making a concurrent userspec: invocation rate = %.3fs, period = %.3fs, starting at %d functions and adding "
            "%d concurrents over %d periods",
            self.invocation_rate,
            self.period,
            self.start,
            self.new,
            self.periods,
        )

        ret = {"users": {"user001": self._make_user(0.0, self.start)}}
        ret["users"].update({f"user{i + 1:03}": self._make_user(i * self.period, self.new) for i in range(1, self.periods)})

        return ret

    def __repr__(self):
        return f"ConcurrentSpecMaker({vars(self)})"

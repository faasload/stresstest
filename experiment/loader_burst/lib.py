from dataclasses import asdict, dataclass
from enum import Enum
from os import path
from typing import Any, NamedTuple

import numpy as np
import tomlkit as toml
from tomlkit.items import Null as TOMLNull

EXPERIMENT_NAME = path.basename(path.dirname(__file__))


class ExperimentRunStatus(Enum):
    Success = "success"
    FaaSLoadRunFailure = "faasload-run-failure"
    ExecutionFailure = "execution-failure"
    UserInterrupt = "user-interrupt"

    def __str__(self) -> str:
        return self.value


class InjectionTimingError(NamedTuple):
    timestamp: float
    error: float


@dataclass
class ExperimentRunResult:
    status: ExperimentRunStatus
    injection_timing_errors: list[InjectionTimingError] | None

    def to_toml(self) -> dict[str, Any]:
        self_d = asdict(self)
        self_d["status"] = self.status.value
        if self.injection_timing_errors is None:
            del self_d["injection_timing_errors"]

        return self_d

    @classmethod
    def from_toml(cls, table) -> "ExperimentRunResult":
        return cls(
            status=ExperimentRunStatus(table["status"]),
            injection_timing_errors=[
                InjectionTimingError(timestamp=error[0], error=error[1])
                for error in table["injection_timing_errors"].unwrap()
            ],
        )


@dataclass
class ExperimentRunStatistics:
    min: float
    max: float
    absmin: float
    absmax: float
    absmean: float
    absstdev: float

    @classmethod
    def from_run(cls, run: ExperimentRunResult) -> "ExperimentRunStatistics":
        if run.injection_timing_errors is None:
            raise ValueError("injection timing errors were not collected in this run")

        arr = np.array([err[1] for err in run.injection_timing_errors])
        return cls(
            min=arr.min(),
            max=arr.max(),
            absmin=np.abs(arr).min(),
            absmax=np.abs(arr).max(),
            absmean=np.abs(arr).mean(),
            absstdev=np.abs(arr).std(),
        )

    def to_toml(self) -> dict[str, Any]:
        return asdict(self)


class ExperimentStatus(Enum):
    """Status of the experiment."""

    Success = "success"
    LoadWskAssetsFailure = "load-wsk-assets-failure"
    RunFailure = "run-failure"
    UserInterrupt = "user-interrupt"

    def __str__(self) -> str:
        return self.value


@dataclass
class ExperimentResult:
    """Result of the burst experiment variant."""

    nb_runs: int | None
    runs: list[ExperimentRunResult] | None
    status: ExperimentStatus = ExperimentStatus.Success

    def __str__(self) -> str:
        if self.status is ExperimentStatus.LoadWskAssetsFailure or self.nb_runs is None or self.runs is None:
            return f"ExperimentResult(status={self.status.value})"
        else:
            return f"ExperimentResult(status={self.status.value}, runs={self.nb_runs})"

    def to_toml(self) -> dict[str, Any]:
        self_d = asdict(self)
        if self.nb_runs is None:
            del self_d["nb_runs"]
        if self.runs is None:
            del self_d["runs"]
        else:
            self_d["runs"] = [run.to_toml() for run in self.runs]
        self_d["status"] = self.status.value

        return self_d

    @classmethod
    def from_toml(cls, table) -> "ExperimentResult":
        return cls(
            nb_runs=table.get("nb_runs", TOMLNull()).unwrap(),
            runs=[ExperimentRunResult.from_toml(run) for run in table.get("runs", toml.array())],
            status=ExperimentStatus(table["status"]),
        )

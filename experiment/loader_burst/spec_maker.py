import logging
from typing import Callable

from .lib import EXPERIMENT_NAME


class BurstySpecMaker:
    """Make an injection user spec for the bursty experiment.
    
    The user spec includes only one function under one user, invoked over n burst periods.
    Every burst period p, the function is first invoked at the low rate l, it then starts being invoked at the given
    high rate h.
    The duration of the high invocation phase (i.e., the bursty phase, excluding transitions) is given by the ratio b
    applied to the burst period p.
    The durations of the ramp up and down phases are given by the transition ratio t applied to the burst period p.
    The time spent at the low rate during each burst period is deduced from the rates r and t.
    
    In other words, the trace will look like this (starting with a low phase):
    
      ^ rate
      |
    h +         .-----.            .-----.
      |        /       \\         /       \\
      |       /         \\       /         \\
      |      /           \\     /           \\
    l + ----'             '----'             '     
       |    |t*p| b*p |t*p|    |t*p| b*p |t*p|
       |        p         |        p         |
       |                 n*p                 |

    As can be seen, a constraint is: tp+bp+tp <= p; i.e.: b+2t <= 1.
    However, the durations of the ramp up and down phases may be 0, i.e., you may specify t = 0; this is the default
    value.
    It also follows easily that the time spent at low invocation rate per burst period is (1-b-2t)*p.
    
    Notice that the number of burst periods n can be specified (it defaults to 1), which places a maximum duration on
    the user spec, and thus the injection trace: n*p.
    When building the injection trace from the user spec with FaaSLoad's trace builder, a duration must be specified:
    you may elect to use n*p, or any shorter duration.
    """

    def __init__(
        self,
        action: str,
        low_rate: float,
        high_rate: float,
        nb_periods: int,
        period: float,
        burst_ratio: float,
        transition_ratio: float,
    ):
        self.action = action
        self.low_rate = low_rate
        self.high_rate = high_rate
        self.nb_periods = nb_periods
        self.period = period
        self.burst_ratio = burst_ratio
        self.transition_ratio = transition_ratio

        if self.burst_ratio + 2 * self.transition_ratio > 1:
            raise ValueError("the sum of burst and two transition ratios is greater than 1")

        self.logger = logging.getLogger(f"experiment.{EXPERIMENT_NAME}.{self.__class__.__name__}")

    def make(self) -> dict:
        trans_dur = self.transition_ratio * self.period
        high_dur = self.burst_ratio * self.period
        low_dur = self.period - high_dur - 2 * trans_dur

        trans_rate = (self.high_rate - self.low_rate) / trans_dur

        self.logger.info(
            "making a bursty userspec: transition duration = %.3fs, high rate duration = %.3fs, "
            "low rate duration = %.3fs",
            trans_dur,
            high_dur,
            low_dur,
        )

        def trans_up_iat(_t: float) -> float:
            return 1 / (self.low_rate + trans_rate * _t)

        def trans_down_iat(_t: float) -> float:
            return 1 / (self.high_rate - trans_rate * _t)

        def high_iat(_: float) -> float:
            return round(1 / self.high_rate, 3)

        def low_iat(_: float) -> float:
            return round(1 / self.low_rate, 3)

        def make_burst_subperiod(
            _duration: float, _t0: float, _iat_fn: Callable[[float], float]
        ) -> tuple[list[float], float]:
            _subperiod = []
            _t = _t0

            while _t < _duration:
                _iat = _iat_fn(_t)
                _subperiod.append(_iat)
                _t += _iat

            return _subperiod, _t - _duration

        burst_period = []
        over = 0.0
        for dur, iat_fn in [
            (low_dur, low_iat),
            (trans_dur, trans_up_iat),
            (high_dur, high_iat),
            (trans_dur, trans_down_iat),
        ]:
            if over >= dur:
                self.logger.warning(
                    "the spillover of the previous subperiod is greater than the duration of the current subperiod "
                    "(generated with IAT function %s); "
                    "this may be a sign that the period is too short, or a subperiod rate is too low",
                    iat_fn.__name__,
                )

            subperiod, over = make_burst_subperiod(dur, over, iat_fn)
            burst_period.extend(subperiod)

        return {
            "users": {
                "user001": {
                    "memory-profile": "smart",
                    "nb-inputs": {
                        "image": 1,
                        "video": 1,
                        "audio": 1,
                        "none": 1,
                    },
                    "actions": [
                        {
                            "name": self.action,
                            "times": burst_period * self.nb_periods,
                        },
                    ],
                },
            },
        }

    def __repr__(self):
        return f"BurstySpecMaker({vars(self)})"

import os
from dataclasses import asdict, dataclass
from typing import Any

import tomlkit

from experiment import ActionConfiguration, GlobalConfiguration

from .lib import EXPERIMENT_NAME


@dataclass
class InjectionTraceConfiguration:
    low_rate: float
    high_rate: float
    burst_ratio: float
    transition_ratio: float

    def __str__(self) -> str:
        return (
            f"low rate: {self.low_rate:.3f}inv./s, high rate: {self.high_rate:.3f}inv./s, "
            f"ratio of low rate: {1 - self.burst_ratio - 2 * self.transition_ratio:.1%}, "
            f"ratio of high rate: {self.burst_ratio:.1%}, ratio of transition rates: {self.transition_ratio:.1%}"
        )

    @classmethod
    def from_toml(cls, table) -> "InjectionTraceConfiguration":
        return cls(**table.unwrap())

    def to_toml(self) -> dict[str, Any]:
        return asdict(self)


@dataclass
class ExperimentConfiguration:
    name: str
    runs: int
    injection_trace: InjectionTraceConfiguration
    action: ActionConfiguration

    userspec_fn: str
    injection_traces_dn: str
    authkeys_dn: str

    def __str__(self) -> str:
        return f"{self.name}: runs: {self.runs}, injection trace: {self.injection_trace}, action: {self.action}"

    @classmethod
    def from_toml(cls, table) -> "ExperimentConfiguration":
        return cls(
            name=table["name"].unwrap(),
            runs=table["runs"].unwrap(),
            injection_trace=InjectionTraceConfiguration.from_toml(table["injection_trace"]),
            action=ActionConfiguration.from_toml(table["action"]),
            userspec_fn=table["userspec_fn"].unwrap(),
            injection_traces_dn=table["injection_traces_dn"].unwrap(),
            authkeys_dn=table["authkeys_dn"].unwrap(),
        )

    def to_toml(self) -> dict[str, Any]:
        self_d = asdict(self)
        self_d["injection_trace"] = self.injection_trace.to_toml()
        self_d["action"] = self.action.to_toml()

        return self_d


@dataclass
class ExperimentParameters:
    """Gather the configuration for the bursty loader stresstest, and its runtime parameters."""

    burst: ExperimentConfiguration

    periods: int
    period: float

    def __str__(self) -> str:
        return f"periods: {self.periods}, period: {self.period:.3f}s, {self.burst}"

    @classmethod
    def from_toml(cls, table) -> "ExperimentParameters":
        return cls(
            burst=ExperimentConfiguration.from_toml(table["burst"]),
            periods=table["periods"].unwrap(),
            period=table["period"].unwrap(),
        )

    def to_toml(self) -> dict[str, Any]:
        self_d = asdict(self)
        self_d["burst"] = self.burst.to_toml()

        return self_d


def load_config(config_dp: str = "config") -> ExperimentConfiguration:
    with open(os.path.join(config_dp, EXPERIMENT_NAME, "config.toml"), "r") as config_f:
        return ExperimentConfiguration.from_toml(tomlkit.load(config_f))


def load_parameters(global_config: GlobalConfiguration, results_dp: str) -> ExperimentParameters:
    with open(os.path.join(results_dp, global_config.filenames.experiment_parameters), "r") as params_f:
        return ExperimentParameters.from_toml(tomlkit.load(params_f))

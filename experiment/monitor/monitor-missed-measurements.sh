#!/usr/bin/env bash

# Resolutions of monitoring (polling period, in seconds)
MONITORING_RESOS="0.01 0.05 0.1 0.5"

# Image of container to spawn during the test
CONTAINER_IMAGE="faasload/stresstest-sleep"
# Template of spawned containers' names
CONTAINER_NAME_TMPL="faasload-stresstest-%d"

# Path to FaaSLoad's Docker Compose main file
FAASLOAD_COMPOSE="../faasload/docker/compose.yml"

# Directory to store results
RESULTS_DIR="results/monitor-missed-measurements"

die() {
    echo "ERROR: $*" 1>&2
    exit 1
}

docker_compose() {
    docker compose --file "$FAASLOAD_COMPOSE" --file docker/compose.configs.yml --project-name faasload-stresstest\
        "$@"
}

container_name() {
    # shellcheck disable=SC2059
    printf "$CONTAINER_NAME_TMPL" "$i"
}

echo "FaaSLoad -- Monitor stress test: missed measurements"
echo

if [ $# -lt 2 ]; then
    echo "Usage: $0 N DELAY"
    echo
    echo "* N: run up to this number of stress test containers"
    echo "* DELAY: time to wait (seconds) before running the next container"
    exit 2
fi >&2

n="$1"
delay="$2"

echo "Monitoring resolutions: ${MONITORING_RESOS// /, } seconds"
echo "For each monitoring resolution, going from 1 to $n containers"
echo "Running a new container every ${delay}s"
echo

# Make sure the image is locally available
docker images | grep -q "$CONTAINER_IMAGE" ||
    die "stress test Docker image \"$CONTAINER_IMAGE\" is not available in the local registry"

mkdir -p "$RESULTS_DIR"

echo -e "$n\n$delay" > "$RESULTS_DIR/config"

for mon_reso in $MONITORING_RESOS; do
    echo "=== Stress test for monitoring resolution ${mon_reso}s begins"

    reso_results_dir="$RESULTS_DIR/$mon_reso"

    mkdir "$reso_results_dir" ||
        die "failed creating results directory \"$reso_results_dir\" for monitoring resolution ${mon_reso}s"

    sed 's/{{resolution}}/'"$mon_reso"'/' config/monitor.yml.tmpl > config/monitor.yml
    sed 's!{{monitorcfg_path}}!'"$PWD/config/monitor.yml"'!' docker/compose.configs.yml.tmpl > docker/compose.configs.yml

    echo "--- Starting monitor"
    docker_compose up --detach monitor ||
        die "failed starting monitor for monitoring resolution ${mon_reso}s"
    sleep 1
    echo "--- Started monitor"

    echo "--- Stress test begins"
    echo -n "Running container "
    for i in $(seq 1 "$n"); do
        echo -n "$i "

        docker run --detach --rm --name "$(container_name "$i")" "$CONTAINER_IMAGE" > /dev/null

        sleep "$delay"
    done
    echo
    echo "--- Stress test ends"

    echo "--- Stopping containers"
    for i in $(seq 1 "$n"); do container_name "$i"; echo; done | xargs docker stop ||
        die "failed stopping all containers for monitoring resolution ${mon_reso}s"
    echo "--- Stopped containers"

    echo "--- Saving monitor logs"
    # docker compose outputs log lines with a starting space character: use sed to remove it
    docker_compose logs --no-color --no-log-prefix monitor | sed 's/^\s*//' > "$reso_results_dir/monitor.log" 2>&1 ||
        die "failed saving monitor logs"
    echo "--- Saved monitor logs"

    echo "--- Stopping monitor"
    docker_compose down monitor ||
        die "failed stopping monitor for monitoring resolution ${mon_reso}s"
    sleep 1
    echo "--- Stopped monitor"

    echo "=== Stress test for monitoring resolution ${mon_reso}s ends"
done
#! /usr/bin/env python3

from __future__ import annotations

import pickle
import re
import sys
from typing import TextIO

if __name__ == '__main__':
    sys.path.insert(0, '.')

from resultsparser import (ContainerDatapoint, list_resolution_dirs, make_log_filename, make_parsed_data_filename,
                           read_monitor_missed_meas_config)

LOGMSG_RE = re.compile(r'^\[[^]]+]\[(\w+)]\[([\w.]+)]\s*(.*)$')
RUNCONTAINER_RE = re.compile(r'^run monitor for container ([\da-f]+)$')
DELETECONTAINER_RE = re.compile(r'^scheduled deletion of monitor for container ([\da-f]+)$')
MISSEDPERF_RE = re.compile(r'^\[monitor-([\da-f]{12})]\s*no perf measure available for the monitoring round at [\d.]+'
                           r' \(total missing: (\d+)/\d+, [\d.]+%\)$')
OUTOFDATEPERF_RE = re.compile(r'^\[monitor-([\da-f]{12})]\s*perf measure is out-of-date, need a new one$')
MISSEDMEAS_RE = re.compile(r'^\[monitor-([\da-f]{12})]\s*missed \d+ measurements between [\d.]+ and [\d.]+'
                           r' \(total missed: (\d+)/\d+, [\d.]+%\)$')


class SkippedLogLines:
    malformed_log: int = 0
    run_container: int = 0
    missed_meas: int = 0
    missed_perf: int = 0

    @property
    def total(self) -> int:
        return self.malformed_log + self.run_container + self.missed_perf + self.missed_meas

    def __str__(self) -> str:
        return (f'malformed: {self.malformed_log}, run container: {self.run_container}, missed measurements: '
                f'{self.missed_meas}, missed perf measures: {self.missed_perf} (total: {self.total})')


def parse_log(log_file: TextIO) -> dict[str, dict[int, ContainerDatapoint]]:
    # Key: container ID
    # Value: dict
    #   Key: number of container
    #   Val: tuple (number missing measurements, number missed perf measurements) at this number of container (maximum)
    d = {}
    cur_nb_cont = 0

    skipped = SkippedLogLines()

    for line in log_file:
        line_regexed = LOGMSG_RE.match(line)

        if not line_regexed or len(line_regexed.groups()) != 3:
            skipped.malformed_log += 1
            continue

        log_level = line_regexed[1]
        log_location = line_regexed[2]
        log_msg = line_regexed[3]

        if log_level == 'INFO':
            if log_location == 'dockeroperations':
                msg_regexed = RUNCONTAINER_RE.match(log_msg)

                if not msg_regexed or len(msg_regexed.groups()) != 1:
                    # If this is the "delete container" message, just ignore.
                    if not DELETECONTAINER_RE.match(log_msg):
                        # Display an example of skipped line the first time one is encountered.
                        if skipped.run_container == 0:
                            print(f'Skipped run container log line, example:\n{log_msg}')
                        skipped.run_container += 1
                    continue

                cont_id = msg_regexed[1][:12]

                cur_nb_cont += 1
                d[cont_id] = {}

                # Create a datapoint for all currently existing containers at this
                # number of containers.
                # Thus, the newly created container does not have any datapoint for
                # the previous numbers of containers, and the previously created
                # containers continue to have datapoints for the current and future
                # numbers of containers.
                for cont_data in d.values():
                    cont_data[cur_nb_cont] = ContainerDatapoint()
        elif log_level == 'WARNING':
            if log_location == 'monitor.perf':
                # missed perf measurement
                msg_regexed = MISSEDPERF_RE.match(log_msg)

                if not msg_regexed or len(msg_regexed.groups()) != 2:
                    # If this is the "perf measure is out-of-date" message, just ignore.
                    if not OUTOFDATEPERF_RE.match(log_msg):
                        # Display an example of skipped line the first time one is encountered.
                        if skipped.missed_perf == 0:
                            print(f'Skipped missed perf measure log line, example:\n{log_msg}')
                        skipped.missed_perf += 1
                    continue

                cont_id = msg_regexed[1]
                missed_perf = int(msg_regexed[2])

                d[cont_id][cur_nb_cont].missed_perf = missed_perf
            elif log_location == 'monitor':
                # missed measurement
                msg_regexed = MISSEDMEAS_RE.match(log_msg)

                if not msg_regexed or len(msg_regexed.groups()) != 2:
                    # Display an example of skipped line the first time one is encountered.
                    if skipped.missed_meas == 0:
                        print(f'Skipped missed measurement log line, example:\n{log_msg}')
                    skipped.missed_meas += 1
                    continue

                cont_id = msg_regexed[1]
                missed_meas = int(msg_regexed[2])

                d[cont_id][cur_nb_cont].missed_meas = missed_meas

    print(f'Skipped {skipped} lines because regexes did not match')

    return d


def cumulate(d: dict[str, dict[int, ContainerDatapoint]]):
    """Cumulate missed (perf) measurements for a given container over the number of containers.

    For a given container, the logs may not show any missed measurement at a given number of containers.
    Cumulate the number of missed measurements for a given container over the duration of the experiment.
    Cumulating simply means keeping the maximum number of missed measurements over the number of containers.
    """
    for cont_data in d.values():
        dp_iter = iter(cont_data.values())

        prev_dp = next(dp_iter)

        try:
            while True:
                cur_dp = next(dp_iter)
                cur_dp.cumulate_missed(prev_dp)
                prev_dp = cur_dp
        except StopIteration:
            pass


def fill_expected(d: dict[str, dict[int, ContainerDatapoint]], monitoring_resolution: float, delay: float):
    # + 1 to count the first one at t = 0
    expected_per_container = delay // monitoring_resolution + 1

    for cont_data in d.values():
        for nb_cont, dp in cont_data.items():
            dp.expected = nb_cont * expected_per_container


def main():
    config = read_monitor_missed_meas_config()

    print(f'Parsing results for stress test: monitor missed measurements (up to {config.n} containers, new container '
          f'every {config.delay}s)')

    for mon_reso_resdir in list_resolution_dirs():
        print(f'Parsing monitor logs for monitoring resolution {mon_reso_resdir}')

        mon_reso = float(mon_reso_resdir)
        with open(make_log_filename(mon_reso_resdir), 'r') as f:
            data = parse_log(f)

        cumulate(data)
        fill_expected(data, mon_reso, config.delay)

        with open(make_parsed_data_filename(mon_reso_resdir), 'wb') as f:
            pickle.dump(data, f)


if __name__ == '__main__':
    main()

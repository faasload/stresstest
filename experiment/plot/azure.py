#! /bin/env python3

import logging
import sys
from math import ceil
from os import path

import matplotlib.pyplot as plt
import numpy as np
import tomlkit
from cycler import cycler
from matplotlib import ticker

from experiment import configure_logging
from experiment.azure.config import load_parameters
from experiment.azure.lib import EXPERIMENT_NAME, ExperimentResult, ExperimentRunStatus, ExperimentStatus
from experiment.config import load_global_config

from .config import load_config, make_plot_dp

# Landscape A4 sheet.
FIG_SZ = (29.7 / 2.54, 21 / 2.54)
FIG_COLOR_MAP = plt.color_sequences["Set1"]
plt.rcParams.update({"font.size": 18, "axes.linewidth": 2, "lines.linewidth": 2})
plt.rcParams["axes.prop_cycle"] = cycler(color=FIG_COLOR_MAP)


class Plotter:
    def __init__(self, results_dp: str):
        self.results_dp = path.realpath(results_dp)

        config = load_config()
        global_config = load_global_config(config_dp=self.results_dp)
        configure_logging()

        self.logger = logging.getLogger("plot.azure")

        self.params = load_parameters(global_config, self.results_dp)

        self.logger.info("loaded experiment parameters")
        self.logger.debug("%s", self.params)

        self.plot_dp = make_plot_dp(config, EXPERIMENT_NAME, path.basename(self.results_dp))

        result_fp = path.join(self.results_dp, global_config.filenames.experiment_result)

        self.logger.info("reading experiment results from `%s`", result_fp)

        with open(result_fp, "r") as result_f:
            self.result = ExperimentResult.from_toml(tomlkit.load(result_f))

    def make_all_plots(self):
        self.logger.info("making plots for Azure trace injection results from `%s`", self.results_dp)

        self.logger.info("making injection timing errors plot")

        plot_fp = path.join(self.plot_dp, "injection-timing-errors.pdf")

        fig, axes = plt.subplots(
            2,
            1,
            figsize=FIG_SZ,
            height_ratios=[2 / 3, 1 / 3],
            layout="tight",
            sharex=True,
        )
        inj_timing_ax = axes[0]
        cpu_usage_ax = axes[1]
        context_switches_ax = cpu_usage_ax.twinx()

        if self.result.nb_runs > 1:
            raise ValueError("plotting more than one run is not supported")
        run = self.result.runs[0]

        if run.status is not ExperimentRunStatus.Success:
            self.logger.warning("run ended with status %s", run.status.value)

        for user, errs in run.injection_timing_errors.items():
            inj_timing_ax.plot(
                [err[0] for err in errs],
                [err[1] for err in errs],
                marker="x",
                linestyle="",
                color=FIG_COLOR_MAP[0],
            )

        # x_step = 100
        # inj_timing_ax.set_xticks(
        #     [i * self.params.period for i in range(0, nb_users, ceil(x_step / self.params.new_concurrents))],
        #     [
        #         self.params.start_concurrents + i * self.params.new_concurrents
        #         for i in range(0, nb_users, ceil(x_step / self.params.new_concurrents))
        #     ],
        # )
        # inj_timing_ax.xaxis.set_minor_locator(
        #     ticker.MultipleLocator(base=ceil(self.params.period * (x_step / 4) / self.params.new_concurrents))
        # )
        # y_nb_steps = 15
        # inj_timing_ax.yaxis.set_major_locator(
        #     ticker.MultipleLocator(
        #         base=ceil(
        #             abs(
        #                 max(err[1] for errs in run.injection_timing_errors.values() for err in errs)
        #                 - min(err[1] for errs in run.injection_timing_errors.values() for err in errs)
        #             )
        #             / y_nb_steps
        #         )
        #     )
        # )
        inj_timing_ax.grid()

        cpu_usage_events = run.perf.events["task-clock"]
        context_switches_events = run.perf.events["context-switches"]
        cpu_usage_ax.stairs(
            [event[1] for event in cpu_usage_events],
            [0.0] + [event[0] for event in cpu_usage_events],
            color=FIG_COLOR_MAP[1],
        )
        # Use negative widths to align with right edge.
        context_switches_ax.bar(
            [event[0] for event in context_switches_events],
            [event[1] for event in context_switches_events],
            [-(ne[0] - pe[0]) for ne, pe in zip(context_switches_events, [(0.0, "osef")] + context_switches_events)],
            align="edge",
            color=FIG_COLOR_MAP[2],
        )

        cpu_usage_ax.yaxis.set_major_locator(ticker.MultipleLocator(base=0.2))
        # y_nb_steps = 8
        # context_switches_ax.yaxis.set_major_locator(
        #     ticker.MultipleLocator(base=ceil(max(event[1] for event in context_switches_events) / y_nb_steps))
        # )
        cpu_usage_ax.grid()

        fig.suptitle("Timing errors and CPU usage when injecting workload trace points")

        if self.result.status is not ExperimentStatus.Success:
            inj_timing_ax.text(
                0.5,
                0.5,
                f"Experiment exit status: {self.result.status.value}",
                horizontalalignment="center",
                transform=inj_timing_ax.transAxes,
                rotation=np.arctan(inj_timing_ax.bbox.height / inj_timing_ax.bbox.width) * 180 / np.pi,
                rotation_mode="anchor",
                color="red",
                backgroundcolor="white",
            )
        if run.perf.multiplexing_warning:
            cpu_usage_ax.text(
                0.5,
                0.5,
                "perf multiplexed hardware counters",
                horizontalalignment="center",
                transform=cpu_usage_ax.transAxes,
                rotation=np.arctan(cpu_usage_ax.bbox.height / cpu_usage_ax.bbox.width) * 180 / np.pi,
                rotation_mode="anchor",
                color="red",
                backgroundcolor="white",
            )

        inj_timing_ax.set_ylabel("Timing error (s)")
        cpu_usage_ax.set_ylabel("CPU usage (CPU)")
        context_switches_ax.set_ylabel("Context switches")
        cpu_usage_ax.set_xlabel("Time (s, ?)")

        fig.savefig(plot_fp, bbox_inches="tight")

        self.logger.info("made plot at `%s`", plot_fp)


def main():
    try:
        results_dp = sys.argv[1]
    except IndexError:
        print(f"Usage: {sys.argv[0]} RESULTS_DP\n\n\tRESULTS_DP: the directory of results to plot", file=sys.stderr)
        sys.exit(2)

    Plotter(results_dp).make_all_plots()


if __name__ == "__main__":
    main()

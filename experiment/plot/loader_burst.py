#! /bin/env python3

import logging
import sys
from os import path
from typing import Generator

import matplotlib.pyplot as plt
import numpy as np
import tomlkit
from cycler import cycler

from experiment import configure_logging
from experiment.config import load_global_config
from experiment.loader_burst.config import load_parameters
from experiment.loader_burst.lib import (
    EXPERIMENT_NAME,
    ExperimentResult,
    ExperimentRunStatistics,
    ExperimentRunStatus,
    ExperimentStatus,
)

from .config import load_config, make_plot_dp

# Landscape A4 sheet.
FIG_SZ = (29.7 / 2.54, 21 / 2.54)
FIG_COLOR_MAP = plt.color_sequences["Set1"]
plt.rcParams.update({"font.size": 18, "axes.linewidth": 2, "lines.linewidth": 2})
plt.rcParams["axes.prop_cycle"] = cycler(color=FIG_COLOR_MAP)


class Plotter:
    def __init__(self, results_dp: str):
        self.results_dp = path.realpath(results_dp)

        config = load_config()
        global_config = load_global_config(config_dp=self.results_dp)
        configure_logging()

        self.logger = logging.getLogger("plot.loader_burst")

        self.params = load_parameters(global_config, self.results_dp)

        self.logger.info("loaded experiment parameters")
        self.logger.debug("%s", self.params)

        self.plot_dp = make_plot_dp(config, EXPERIMENT_NAME, path.basename(self.results_dp))

        result_fp = path.join(self.results_dp, global_config.filenames.experiment_result)

        self.logger.info("reading experiment results from `%s`", result_fp)

        with open(result_fp, "r") as result_f:
            self.result = ExperimentResult.from_toml(tomlkit.load(result_f))

    def _subperiods(self) -> Generator[tuple[float, float], None, None]:
        trans_dur = self.params.burst.injection_trace.transition_ratio * self.params.period
        high_dur = self.params.burst.injection_trace.burst_ratio * self.params.period
        low_dur = self.params.period - high_dur - 2 * trans_dur

        for burst_i in range(self.params.periods):
            for dur, rate in [
                (low_dur, self.params.burst.injection_trace.low_rate),
                (trans_dur, self.params.burst.injection_trace.high_rate),
                (high_dur, self.params.burst.injection_trace.high_rate),
                (trans_dur, self.params.burst.injection_trace.low_rate),
            ]:
                yield dur, rate

    def _invocation_trace_plot_points(self) -> tuple[list[float], list[float]]:
        x = []
        y = []

        t = 0.0
        x.append(t)
        y.append(self.params.burst.injection_trace.low_rate)
        for dur, rate in self._subperiods():
            t += dur
            x.append(t)
            y.append(rate)

        return x, y

    def make_all_plots(self):
        self.logger.info("making plots for loader burst results from `%s`", self.results_dp)

        self.logger.info("making injection timing errors plot")

        plot_fp = path.join(self.plot_dp, "injection-timing-errors.pdf")

        fig, axes = plt.subplots(2, 1, figsize=FIG_SZ, height_ratios=[2 / 3, 1 / 3], layout="tight", sharex=True)
        inj_timing_ax = axes[0]
        invoc_rate_axes = axes[1]

        invoc_rate_axes.plot(*self._invocation_trace_plot_points())

        t = 0.0
        for dur, _ in self._subperiods():
            t += dur

            invoc_rate_axes.axvline(
                t,
                linewidth=3,
                linestyle="--",
                color=FIG_COLOR_MAP[-1],
            )
            inj_timing_ax.axvline(
                t,
                linewidth=3,
                linestyle="--",
                color=FIG_COLOR_MAP[-1],
            )

        for i, run in enumerate(self.result.runs):
            if run.status is not ExperimentRunStatus.Success:
                self.logger.warning("run #%d ended with status %s", i + 1, run.status.value)

            inj_timing_ax.plot(
                [err[0] for err in run.injection_timing_errors],
                [err[1] for err in run.injection_timing_errors],
                marker="x",
                linestyle="",
                label=f"run #{i+1}",
            )

        fig.suptitle("Timing errors when injecting workload trace points")

        if self.result.status is not ExperimentStatus.Success:
            invoc_rate_axes.text(
                0.5,
                0.5,
                f"Experiment exit status: {self.result.status.value}",
                horizontalalignment="center",
                transform=invoc_rate_axes.transAxes,
                rotation=np.arctan(invoc_rate_axes.bbox.height / invoc_rate_axes.bbox.width) * 180 / np.pi,
                rotation_mode="anchor",
                color="red",
                backgroundcolor="white",
            )
            inj_timing_ax.text(
                0.5,
                0.5,
                f"Experiment exit status: {self.result.status.value}",
                horizontalalignment="center",
                transform=inj_timing_ax.transAxes,
                rotation=np.arctan(inj_timing_ax.bbox.height / inj_timing_ax.bbox.width) * 180 / np.pi,
                rotation_mode="anchor",
                color="red",
                backgroundcolor="white",
            )

        invoc_rate_axes.set_xlabel("Time (s)")
        invoc_rate_axes.set_ylabel("Invocation rate (inv./s)", labelpad=35)
        inj_timing_ax.set_ylabel("Timing error (s)")

        fig.savefig(plot_fp, bbox_inches="tight")

        self.logger.info("made plot at `%s`", plot_fp)

    def write_stats(self):
        self.logger.info("computing stats for loader burst results from `%s`", self.results_dp)

        stats_fp = path.join(self.plot_dp, "stats.toml")

        stats = dict(runs=[ExperimentRunStatistics.from_run(run).to_toml() for run in self.result.runs])

        with open(stats_fp, "w") as stats_f:
            tomlkit.dump(stats, stats_f)

        self.logger.info("wrote stats at `%s`", stats_fp)


def main():
    try:
        results_dp = sys.argv[1]
    except IndexError:
        print(f"Usage: {sys.argv[0]} RESULTS_DP\n\n\tRESULTS_DP: the directory of results to plot", file=sys.stderr)
        sys.exit(2)

    harry = Plotter(results_dp)
    harry.make_all_plots()
    harry.write_stats()


if __name__ == "__main__":
    main()

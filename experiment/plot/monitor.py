#TODO This is all dumped from a Jupyter notebook.

# NOT PART OF THE EXPERIMENT FRAMEWORK
# NOT USED IN FAASLOAD'S OPODIS PAPER
# Just saved here for posterity

from __future__ import annotations

import os
from collections import namedtuple
from collections.abc import Iterator
from dataclasses import dataclass
from typing import Type

RESULTS_DIR = 'results'
MONITOR_MISSED_MEAS_DIR = os.path.join(RESULTS_DIR, 'monitor-missed-measurements_nocgroups')


@dataclass
class ContainerDatapoint:
    missed_meas: int = 0
    missed_perf: int = 0
    expected: int = 0

    def cumulate_missed(self, other: 'ContainerDatapoint'):
        self.missed_meas = max(self.missed_meas, other.missed_meas)
        self.missed_perf = max(self.missed_perf, other.missed_perf)


MonitorMissedMeasurementsConfig: Type['MonitorMissedMeasurementsConfig'] = namedtuple(
    'MonitorMissedMeasurementsConfig',
    ['n', 'delay'])


def read_monitor_missed_meas_config() -> MonitorMissedMeasurementsConfig:
    with open(os.path.join(MONITOR_MISSED_MEAS_DIR, 'config'), 'r') as config_file:
        return MonitorMissedMeasurementsConfig(
            n=int(next(config_file)),
            delay=float(next(config_file)))


def make_log_filename(reso_dir: str | os.PathLike[str]) -> str:
    return os.path.join(MONITOR_MISSED_MEAS_DIR, reso_dir, 'monitor.log')


def make_parsed_data_filename(reso_dir: str | os.PathLike[str]) -> str:
    return os.path.join(MONITOR_MISSED_MEAS_DIR, reso_dir, 'parsed.dat')


def list_resolution_dirs() -> Iterator[str]:
    yield from [
        entry for entry in os.listdir(MONITOR_MISSED_MEAS_DIR) if
        os.path.isdir(os.path.join(MONITOR_MISSED_MEAS_DIR, entry))]

from __future__ import annotations

import os
import pickle
from collections.abc import Callable, Iterator

import pandas as pd

from resultsparser import (ContainerDatapoint, list_resolution_dirs, make_parsed_data_filename,
                           read_monitor_missed_meas_config)


def get_datapoints_at(nb_cont: int, reso_data: dict[str, dict[int, ContainerDatapoint]]) -> Iterator[
    (str, ContainerDatapoint)]:
    for cont, cont_data in reso_data.items():
        try:
            yield cont, cont_data[nb_cont]
        except KeyError:
            pass


def make_summed_missed_df(missed_data: dict[float, dict[str, dict[int, ContainerDatapoint]]],
                          get_data: Callable[[ContainerDatapoint], int],
                          max_nb_cont: int) -> pd.DataFrame:
    """Make the DataFrame of missed measurements, summed over all containers for each number of containers."""
    return pd.DataFrame({reso: pd.Series(
        {nb_cont: pd.Series(get_data(dp) for _, dp in get_datapoints_at(nb_cont, mon_reso_data)).sum()
         for nb_cont in range(1, max_nb_cont + 1)}
    ) for reso, mon_reso_data in missed_data.items()})


def make_relative_summed_missed_df(missed_data: dict[float, dict[str, dict[int, ContainerDatapoint]]],
                                   get_data: Callable[[ContainerDatapoint], int],
                                   max_nb_cont: int) -> pd.DataFrame:
    """Make the DataFrame of missed measurements, summed over all containers, relative with the expected number of measurements, for each number of containers."""
    d = {}

    for reso, mon_reso_data in missed_data.items():
        d_reso = {}

        for nb_cont in range(1, max_nb_cont + 1):
            datapoints = list(get_datapoints_at(nb_cont, mon_reso_data))

            d_reso[nb_cont] = pd.Series(get_data(dp) for _, dp in datapoints).sum() / pd.Series(
                dp.expected for _, dp in datapoints).sum()

        d[reso] = pd.Series(d_reso)

    return pd.DataFrame(d)


def make_per_cont_missed_df(missed_data: dict[float, dict[str, dict[int, ContainerDatapoint]]],
                            get_data: Callable[[ContainerDatapoint], int],
                            max_nb_cont: int) -> pd.DataFrame:
    d = {}
    for reso, mon_reso_data in missed_data.items():
        # d[str(reso) + '_mean'] = pd.Series({
        #     nb_cont: pd.Series(get_data(dp) for _, dp in get_datapoints_at(nb_cont, mon_reso_data)).mean()
        #     for nb_cont in range(1, max_nb_cont + 1)})
        # d[str(reso) + '_std'] = pd.Series({
        #     nb_cont: pd.Series(get_data(dp) for _, dp in get_datapoints_at(nb_cont, mon_reso_data)).std()
        #     for nb_cont in range(1, max_nb_cont + 1)})
        d[str(reso) + '_median'] = pd.Series({
            nb_cont: pd.Series(get_data(dp) for _, dp in get_datapoints_at(nb_cont, mon_reso_data)).median()
            for nb_cont in range(1, max_nb_cont + 1)})
        d[str(reso) + '_q10'] = pd.Series({
            nb_cont: pd.Series(get_data(dp) for _, dp in get_datapoints_at(nb_cont, mon_reso_data)).quantile(0.1)
            for nb_cont in range(1, max_nb_cont + 1)})
        d[str(reso) + '_q90'] = pd.Series({
            nb_cont: pd.Series(get_data(dp) for _, dp in get_datapoints_at(nb_cont, mon_reso_data)).quantile(0.9)
            for nb_cont in range(1, max_nb_cont + 1)})
        # d[str(reso) + '_max'] = pd.Series({
        #     nb_cont: pd.Series(get_data(dp) for _, dp in get_datapoints_at(nb_cont, mon_reso_data)).max()
        #     for nb_cont in range(1, max_nb_cont + 1)})

    return pd.DataFrame(d)


def make_per_cont_relative_missed_df(missed_data: dict[float, dict[str, dict[int, ContainerDatapoint]]],
                                     get_data: Callable[[ContainerDatapoint], int],
                                     max_nb_cont: int) -> pd.DataFrame:
    d = {}
    for reso, mon_reso_data in missed_data.items():
        # d[str(reso) + '_mean'] = pd.Series({
        #     nb_cont: pd.Series(get_data(dp) / dp.expected for _, dp in get_datapoints_at(nb_cont, mon_reso_data)).mean()
        #     for nb_cont in range(1, max_nb_cont + 1)})
        # d[str(reso) + '_std'] = pd.Series({
        #     nb_cont: pd.Series(get_data(dp) / dp.expected for _, dp in get_datapoints_at(nb_cont, mon_reso_data)).std()
        #     for nb_cont in range(1, max_nb_cont + 1)})
        d[str(reso) + '_median'] = pd.Series({
            nb_cont: pd.Series(
                get_data(dp) / dp.expected for _, dp in get_datapoints_at(nb_cont, mon_reso_data)).median()
            for nb_cont in range(1, max_nb_cont + 1)})
        d[str(reso) + '_q10'] = pd.Series({
            nb_cont: pd.Series(
                get_data(dp) / dp.expected for _, dp in get_datapoints_at(nb_cont, mon_reso_data)).quantile(0.1)
            for nb_cont in range(1, max_nb_cont + 1)})
        d[str(reso) + '_q90'] = pd.Series({
            nb_cont: pd.Series(
                get_data(dp) / dp.expected for _, dp in get_datapoints_at(nb_cont, mon_reso_data)).quantile(0.9)
            for nb_cont in range(1, max_nb_cont + 1)})
        # d[str(reso) + '_max'] = pd.Series({
        #     nb_cont: pd.Series(get_data(dp) / dp.expected for _, dp in get_datapoints_at(nb_cont, mon_reso_data)).max()
        #     for nb_cont in range(1, max_nb_cont + 1)})

    return pd.DataFrame(d)


FIGS_DIR = 'figs'
os.makedirs(FIGS_DIR, exist_ok=True)

config = read_monitor_missed_meas_config()

data = {}
for mon_reso_resdir in list_resolution_dirs():
    print(f'Reading parsed data for monitoring resolution {mon_reso_resdir}')

    with open(make_parsed_data_filename(mon_reso_resdir), 'rb') as f:
        data[mon_reso_resdir] = pickle.load(f)

import matplotlib.pyplot as plt

summed_missed_meas_df = make_summed_missed_df(data, lambda dp: dp.missed_meas, config.n)
summed_rel_missed_meas_df = make_relative_summed_missed_df(data, lambda dp: dp.missed_meas, config.n)

# Slide size: 28x15.75cm
# Figure space on slide:
# * 13.27x10.40cm in double column
# * 27.20x10.40cm in single column
fig, axs = plt.subplots(1, 2, figsize=(27.20 / 2.54, 10.40 / 2.54), dpi=100)

fig.suptitle('Scalability of the monitor for measurements (summed over containers)')

ax = axs[0]
ax.grid(axis='y', which='major')
ax.grid(axis='y', which='minor', linewidth=0.2)
ax.set_yscale('log')
for col_label in sorted(data.keys()):
    ax.plot(summed_missed_meas_df[col_label][summed_missed_meas_df[col_label] > 0], label=col_label)
ax.set_ylim(bottom=1)
ax.set_xlim(left=0)
ax.set_xlabel('Concurrent containers')
ax.set_ylabel('Missed measurements (sum; log)')

ax = axs[1]
ax.grid(axis='y', which='major')
for col_label in sorted(data.keys()):
    ax.plot(summed_rel_missed_meas_df[col_label], label=col_label)
ax.set_ylim(top=1)
ax.set_xlim(left=0)
ax.legend(loc='upper left', title='Resolution (s)', ncol=3)
ax.set_xlabel('Concurrent containers')
ax.set_ylabel('Missed measurements (relative sum)')

import matplotlib.pyplot as plt

per_cont_missed_meas_df = make_per_cont_missed_df(data, lambda dp: dp.missed_meas, config.n)
per_cont_rel_missed_meas_df = make_per_cont_relative_missed_df(data, lambda dp: dp.missed_meas, config.n)

# Slide size: 28x15.75cm
# Figure space on slide:
# * 13.27x10.40cm in double column
# * 27.20x10.40cm in single column
fig, axs = plt.subplots(1, 2, figsize=(27.20 / 2.54, 10.40 / 2.54), dpi=100)

fig.suptitle('Scalability of the monitor for measurements (per container)')

ax = axs[0]
ax.grid(axis='y', which='major')
ax.grid(axis='y', which='minor', linewidth=0.2)
ax.set_yscale('log')
for col_label in sorted(data.keys()):
    # median_positive = per_cont_missed_meas_df[per_cont_missed_meas_df[str(col_label) + '_median'] > 0]
    ax.fill_between(per_cont_missed_meas_df.index, per_cont_missed_meas_df[str(col_label) + '_q10'],
                    per_cont_missed_meas_df[str(col_label) + '_q90'], alpha=0.25)
    # ax.fill_between(per_cont_missed_meas_df.index, per_cont_missed_meas_df[str(col_label) + '_median'], per_cont_missed_meas_df[str(col_label) + '_max'], alpha=0.25)
    # ax.fill_between(per_cont_missed_meas_df.index, per_cont_missed_meas_df[str(col_label) + '_mean'] - per_cont_missed_meas_df[str(col_label) + '_std'], per_cont_missed_meas_df[str(col_label) + '_mean'] + per_cont_missed_meas_df[str(col_label) + '_std'], alpha=0.25)
    ax.plot(per_cont_missed_meas_df[str(col_label) + '_median'], label=col_label)
    # ax.plot(per_cont_missed_meas_df[str(col_label) + '_mean'], label=col_label)
ax.set_ylim(bottom=1)
ax.set_xlim(left=0)
ax.set_xlabel('Concurrent containers')
ax.set_ylabel('Missed measurements (q10, q50, q90; log)')

ax = axs[1]
ax.grid(axis='y', which='major')
for col_label in sorted(data.keys()):
    # median_positive = per_cont_rel_missed_meas_df[per_cont_rel_missed_meas_df[str(col_label) + '_median'] > 0]
    ax.fill_between(per_cont_rel_missed_meas_df.index, per_cont_rel_missed_meas_df[str(col_label) + '_q10'],
                    per_cont_rel_missed_meas_df[str(col_label) + '_q90'], alpha=0.25)
    # ax.fill_between(per_cont_rel_missed_meas_df.index, per_cont_rel_missed_meas_df[str(col_label) + '_median'], per_cont_rel_missed_meas_df[str(col_label) + '_max'], alpha=0.25)
    # ax.fill_between(per_cont_rel_missed_meas_df.index, per_cont_rel_missed_meas_df[str(col_label) + '_mean'] - per_cont_rel_missed_meas_df[str(col_label) + '_std'], per_cont_rel_missed_meas_df[str(col_label) + '_mean'] + per_cont_rel_missed_meas_df[str(col_label) + '_std'], alpha=0.25)
    ax.plot(per_cont_rel_missed_meas_df[str(col_label) + '_median'], label=col_label)
    # ax.plot(per_cont_rel_missed_meas_df[str(col_label) + '_mean'], label=col_label)
ax.set_ylim(top=1)
ax.set_xlim(left=0)
ax.legend(loc='upper left', title='Resolution (s)', ncol=2)
ax.set_xlabel('Concurrent containers')
ax.set_ylabel('Missed measurements (relative; q10, q50, q90)')


import matplotlib.pyplot as plt

summed_missed_perf_df = make_summed_missed_df(data, lambda dp: dp.missed_perf, config.n)
summed_rel_missed_perf_df = make_relative_summed_missed_df(data, lambda dp: dp.missed_perf, config.n)

# Slide size: 28x15.75cm
# Figure space on slide:
# * 13.27x10.40cm in double column
# * 27.20x10.40cm in single column
fig, axs = plt.subplots(1, 2, figsize=(27.20 / 2.54, 10.40 / 2.54), dpi=100)

fig.suptitle('Scalability of the monitor for perf measures (summed over containers)')

ax = axs[0]
ax.grid(axis='y', which='major')
ax.grid(axis='y', which='minor', linewidth=0.2)
ax.set_yscale('log')
for col_label in sorted(data.keys()):
    ax.plot(summed_missed_perf_df[col_label][summed_missed_perf_df[col_label] > 0], label=col_label)
ax.set_ylim(bottom=1)
ax.set_xlim(left=0)
ax.set_xlabel('Concurrent containers')
ax.set_ylabel('Missed measures (sum; log)')

ax = axs[1]
ax.grid(axis='y', which='major')
for col_label in sorted(data.keys()):
    ax.plot(summed_rel_missed_perf_df[col_label], label=col_label)
ax.set_ylim(top=1)
ax.set_xlim(left=0)
ax.legend(title='Resolution')
ax.set_xlabel('Concurrent containers')
ax.set_ylabel('Missed measures (relative sum)')

import matplotlib.pyplot as plt

per_cont_missed_perf_df = make_per_cont_missed_df(data, lambda dp: dp.missed_perf, config.n)
per_cont_rel_missed_perf_df = make_per_cont_relative_missed_df(data, lambda dp: dp.missed_perf, config.n)

# Slide size: 28x15.75cm
# Figure space on slide:
# * 13.27x10.40cm in double column
# * 27.20x10.40cm in single column
fig, axs = plt.subplots(1, 2, figsize=(27.20 / 2.54, 10.40 / 2.54), dpi=100)

fig.suptitle('Scalability of the monitor for perf measures (per container)')

ax = axs[0]
ax.grid(axis='y', which='major')
ax.grid(axis='y', which='minor', linewidth=0.2)
ax.set_yscale('log')
for col_label in sorted(data.keys()):
    # median_positive = per_cont_missed_perf_df[per_cont_missed_perf_df[str(col_label) + '_median'] > 0]
    ax.fill_between(per_cont_missed_perf_df.index, per_cont_missed_perf_df[str(col_label) + '_q10'],
                    per_cont_missed_perf_df[str(col_label) + '_q90'], alpha=0.25)
    # ax.fill_between(per_cont_missed_perf_df.index, per_cont_missed_perf_df[str(col_label) + '_median'], per_cont_missed_perf_df[str(col_label) + '_max'], alpha=0.25)
    # ax.fill_between(per_cont_missed_perf_df.index, per_cont_missed_perf_df[str(col_label) + '_mean'] - per_cont_missed_perf_df[str(col_label) + '_std'], per_cont_missed_perf_df[str(col_label) + '_mean'] + per_cont_missed_perf_df[str(col_label) + '_std'], alpha=0.25)
    ax.plot(per_cont_missed_perf_df[str(col_label) + '_median'], label=col_label)
    # ax.plot(per_cont_missed_perf_df[str(col_label) + '_mean'], label=col_label)
ax.set_ylim(bottom=1)
ax.set_xlim(left=0)
ax.legend(title='Resolution')
ax.set_xlabel('Concurrent containers')
ax.set_ylabel('Missed measures (q10, q50, q90; log)')

ax = axs[1]
ax.grid(axis='y', which='major')
for col_label in sorted(data.keys()):
    # median_positive = per_cont_rel_missed_perf_df[per_cont_rel_missed_perf_df[str(col_label) + '_median'] > 0]
    ax.fill_between(per_cont_rel_missed_perf_df.index, per_cont_rel_missed_perf_df[str(col_label) + '_q10'],
                    per_cont_rel_missed_perf_df[str(col_label) + '_q90'], alpha=0.25)
    # ax.fill_between(per_cont_rel_missed_perf_df.index, per_cont_rel_missed_perf_df[str(col_label) + '_median'], per_cont_rel_missed_perf_df[str(col_label) + '_max'], alpha=0.25)
    # ax.fill_between(per_cont_rel_missed_perf_df.index, per_cont_rel_missed_perf_df[str(col_label) + '_mean'] - per_cont_rel_missed_perf_df[str(col_label) + '_std'], per_cont_rel_missed_perf_df[str(col_label) + '_mean'] + per_cont_rel_missed_perf_df[str(col_label) + '_std'], alpha=0.25)
    ax.plot(per_cont_rel_missed_perf_df[str(col_label) + '_median'], label=col_label)
    # ax.plot(per_cont_rel_missed_perf_df[str(col_label) + '_mean'], label=col_label)
ax.set_ylim(top=1)
ax.set_xlim(left=0)
ax.legend(loc='upper left', title='Resolution')
ax.set_xlabel('Concurrent containers')
ax.set_ylabel('Missed measures (relative; q10, q50, q90)')

from matplotlib import ticker

plt.style.use('dark_background')

with plt.rc_context({
    # "text.usetex": True,
    'font.family': 'sans-serif',
    'font.size': 20,
}):
    fig, axs = plt.subplots(1, 2, figsize=(29.7 / 2.54, 11.36 / 2.54), dpi=100)

    fig.suptitle('Missed measurements. Lower is better.')

    ax = axs[0]
    ax.set_yscale('log')
    ax.yaxis.set_major_locator(ticker.LogLocator(base=10, numticks=10))
    ax.yaxis.set_minor_locator(ticker.LogLocator(base=10, subs='auto', numticks=10))
    ax.grid(axis='y', which='major')
    ax.grid(axis='y', which='minor', linewidth=0.2)
    for col_label in sorted(data.keys()):
        ax.plot(summed_missed_meas_df[col_label][summed_missed_meas_df[col_label] > 0], label=col_label)
    ax.set_ylim(bottom=1)
    ax.set_xlim(left=0)
    ax.set_xlabel('Concurrent containers')
    ax.set_ylabel(r'Sum (log)')
    ax.set_title('Sum over all containers')

    ax = axs[1]
    ax.grid(axis='y', which='major')
    ax.yaxis.set_major_locator(ticker.MultipleLocator(0.25))
    for col_label in sorted(data.keys()):
        # median_positive = per_cont_rel_missed_meas_df[per_cont_rel_missed_meas_df[str(col_label) + '_median'] > 0]
        ax.fill_between(per_cont_rel_missed_meas_df.index, per_cont_rel_missed_meas_df[str(col_label) + '_q10'],
                        per_cont_rel_missed_meas_df[str(col_label) + '_q90'], alpha=0.25)
        # ax.fill_between(per_cont_rel_missed_meas_df.index, per_cont_rel_missed_meas_df[str(col_label) + '_median'], per_cont_rel_missed_meas_df[str(col_label) + '_max'], alpha=0.25)
        # ax.fill_between(per_cont_rel_missed_meas_df.index, per_cont_rel_missed_meas_df[str(col_label) + '_mean'] - per_cont_rel_missed_meas_df[str(col_label) + '_std'], per_cont_rel_missed_meas_df[str(col_label) + '_mean'] + per_cont_rel_missed_meas_df[str(col_label) + '_std'], alpha=0.25)
        ax.plot(per_cont_rel_missed_meas_df[str(col_label) + '_median'], label=col_label)
        # ax.plot(per_cont_rel_missed_meas_df[str(col_label) + '_mean'], label=col_label)
    ax.set_ylim(top=1)
    ax.set_xlim(left=0)
    ax.set_xlabel('Concurrent containers')
    ax.set_ylabel(r'Relative (q10, q50, q90)')
    ax.set_title('Per container')
    ax.legend(loc='upper left', title='Monitoring resolution (s)', ncol=3, fontsize='small', labelspacing=0.3, handlelength=1.5, handletextpad=0.5, columnspacing=1.0)


    fig.tight_layout()

    fig.subplots_adjust(top=0.80)

    fig.savefig(os.path.join(FIGS_DIR, 'monitor-scalability_dark.png'))


import os
from dataclasses import dataclass
from os import PathLike, path

import tomlkit

PLOT_CONFIG_FN = "plot.toml"


@dataclass
class PlotConfiguration:
    path: str

    @classmethod
    def from_toml(cls, table) -> "PlotConfiguration":
        return cls(
            path=path.abspath(table["path"].unwrap()),
        )


def load_config(config_dp: str = "config") -> PlotConfiguration:
    with open(path.join(config_dp, PLOT_CONFIG_FN), "r") as config_f:
        return PlotConfiguration.from_toml(tomlkit.load(config_f))


def make_plot_dp(config: PlotConfiguration, expe_name: str, expe_dn: str | PathLike) -> str:
    ret = path.join(config.path, expe_name, expe_dn)

    # It's fine to update an existing plot, so it is OK if the directory already exists.
    os.makedirs(ret, exist_ok=True)
    return ret

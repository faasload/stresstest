# Stresstest evaluation of FaaSLoad's loader: experiment

## Run

1. follow the instructions in [`vms`](vms/)
2. make sure you have sourced `vms/env`: `source vms/env`
3. deploy OpenWhisk by following the instructions in [`platform-configurations/openwhisk/kubernetes`](platform-configurations/openwhisk/kubernetes)
4. start a PostgreSQL server as advised in [`faasload`](faasload/)
   * do not deploy the Docker monitor, it is piloted by the experiment

## Experimental setup

* Machine: **TODO**

## Overview of evaluation data visualization

### Monitor

* missed measurements over number of monitored containers
    1. summed missed measurements over all containers
        * with twin figure of numbers relative to the sum of the expected number of measurements
    2. median, q10 and q90 of missed measurements over all containers
        * with twin figure relative of numbers relative to the expected numbers of measurements
* missing perf measurements (i.e., collected using the `perf` command)
    * same as above

### Loader

* cumulated absolute delay of injection over frequency of injection
    * one single trace
    * varying number of traces for a given overall frequency

## Performance objectives of FaaSLoad

### Monitor

_**Monitoring at 99 Hz, 13 containers per core**_

Rationale:

* **99 Hz**: this is around once every 10ms, real function duration is seen as low as 100ms
    * also from [Brendan Gregg's Blog on "perf CPU Sampling"](https://www.brendangregg.com/blog/2014-06-22/perf-cpu-sample.html) (non-FaaS specific): 99Hz is a negligible frequency, and it avoids lockstep sampling
* **13 containers per core**: AWS Lambda says 1,769 MiB gives 1 vCPU, and the minimum allocation is 128 MiB, so that is a maximum of about 13 containers per vCPU
    * __applied to the test machine__:
        * with 96 HW threads, this is 1326 containers
        * with only the 48 physical cores, this is 663 containers
    * alternatively: same idea but with the average memory allocation of 256 MiB (so this is close to 7 containers per vCPU, i.e., 663 containers over 96 HW threads, or 332 containers over 48 physical cores)
    * alternatively, directly based on memory: how much minimally sized 128 MiB containers can you put on your server which has XXX GiB of memory?

### Loader

_**361.533 invocations per second**_

Rationale:

* from the data from ["Serverless in the Wild" (Shahrad et al.)](https://www.usenix.org/conference/atc20/presentation/shahrad): the 99.9% highest invocation rate is 361.533 inv./s
    * for reference: the highest invocation rate is 5197 inv./s
    * for reference: at 99% it is 32.4 inv./s


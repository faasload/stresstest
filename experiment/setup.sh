#! /bin/bash

SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"

export KUBECONFIG

# Port of the Kubernetes NodePort to expose OpenWhisk's NGINX API gateway.
# This is the port defined in mycluster.tmpl.yaml when deploying FaaSLoad on Kubernetes.
NGINX_NODEPORT_PORT=31001

# Set OpenWhisk's API host in the configuration.
api_host="$(kubectl get nodes --output jsonpath='{.items[0].status.addresses[?(@.type=="InternalIP")].address}' 2>/dev/null)":$NGINX_NODEPORT_PORT
if [ $? -ne 0 ]; then
    echo "failed getting OpenWhisk API host IP address" >&2
    exit 1
else
    sed 's!{{ API_HOST }}!'"$api_host"'!' "$SCRIPTDIR/../config/global.tmpl.toml" > "$SCRIPTDIR/../config/global.toml"
fi

# Label all nodes as fit to invoke user actions, except the control plane one.
for node in $(kubectl get nodes --selector '!node-role.kubernetes.io/control-plane' --output jsonpath='{.items[*].metadata.name}'); do
    kubectl label node "$node" 'openwhisk-role=invoker'
done

# Set FaaSLoad's expected location for its configuration file to point to ours.
mkdir --parents "$HOME/.config"
rm --force --recursive "$HOME/.config/faasload"
ln --symbolic --force "$(realpath "$SCRIPTDIR/../config/faasload")" "$HOME/.config/faasload"

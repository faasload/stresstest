import os
from dataclasses import asdict, dataclass
from typing import Any

import tomlkit

from experiment import ActionConfiguration, GlobalConfiguration

from .lib import EXPERIMENT_NAME


@dataclass
class ExperimentConfiguration:
    name: str
    runs: int

    action: ActionConfiguration

    conversion_params_fn: str
    authkeys_dn: str

    perf_events: list[str]

    def __str__(self) -> str:
        return f"{self.name}: runs: {self.runs}"

    @classmethod
    def from_toml(cls, table) -> "ExperimentConfiguration":
        return cls(
            name=table["name"].unwrap(),
            runs=table["runs"].unwrap(),
            action=ActionConfiguration.from_toml(table["action"]),
            conversion_params_fn=table["conversion_params_fn"].unwrap(),
            authkeys_dn=table["authkeys_dn"].unwrap(),
            perf_events=table["perf_events"].unwrap(),
        )

    def to_toml(self) -> dict[str, Any]:
        self_d = asdict(self)
        self_d["action"] = self.action.to_toml()

        return self_d


@dataclass
class ExperimentParameters:
    azure: ExperimentConfiguration

    traces_dp: str

    skip_loading_assets: bool = False

    def __str__(self) -> str:
        return f"traces from {self.traces_dp}, {self.azure}"

    @classmethod
    def from_toml(cls, table) -> "ExperimentParameters":
        return cls(
            azure=ExperimentConfiguration.from_toml(table["azure"]),
            traces_dp=table["traces_dp"].unwrap(),
            skip_loading_assets=table.get("skip_loading_assets", False),
        )

    def to_toml(self) -> dict[str, Any]:
        self_d = asdict(self)
        self_d["azure"] = self.azure.to_toml()

        return self_d


def load_config(config_dp: str = "config") -> ExperimentConfiguration:
    with open(os.path.join(config_dp, EXPERIMENT_NAME, "config.toml"), "r") as config_f:
        return ExperimentConfiguration.from_toml(tomlkit.load(config_f))


def load_parameters(global_config: GlobalConfiguration, results_dp: str) -> ExperimentParameters:
    with open(os.path.join(results_dp, global_config.filenames.experiment_parameters), "r") as params_f:
        return ExperimentParameters.from_toml(tomlkit.load(params_f))

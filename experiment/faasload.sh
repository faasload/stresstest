#! /bin/bash

[ $# -lt 1 ] && exit 2

SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"

KUBECONFIG="$(cd "$(dirname "$KUBECONFIG")" && pwd)/$(basename "$KUBECONFIG")"
export KUBECONFIG

export FAASLOAD_PLATFORM="openwhisk"

cd "$SCRIPTDIR/faasload" && pipenv run "$@"

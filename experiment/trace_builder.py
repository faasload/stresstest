# TODO this is mostly copied from FaaSLoad's script trace-builder, but the script is not usable programmatically in one way of the other...
from collections.abc import Iterator
from enum import Enum
from os import PathLike, path
from random import Random
from typing import Any, NamedTuple

from pywhisk.client import read_manifest

INPUT_EXTENSIONS = {
    "image": "jpg",
    "audio": "wav",
    "video": "avi",
}


class IatDistribution(Enum):
    """Recognized distributions of inter-arrival times to build traces.

    * uniform: regular distribution
    * poisson: Poisson distribution of number of invocations, i.e. exponential distribution of IATs
    """

    UNIFORM = "uniform"
    POISSON = "poisson"


def _iat_gen_uniform(rate, rng):
    shift = rng.uniform(0, 1 / rate)

    yield round(shift + 1 / rate, 3)

    while True:
        yield round(1 / rate, 3)


def _iat_gen_poisson(rate, rng):
    while True:
        yield round(rng.expovariate(rate), 3)


IAT_GENERATORS = {IatDistribution.UNIFORM: _iat_gen_uniform, IatDistribution.POISSON: _iat_gen_poisson}


class Trace(Iterator):
    def __init__(self, action_name, action_memory, points):
        self.action_name = action_name
        self.action_memory = action_memory
        self.points = points

    def __iter__(self):
        return self

    def __next__(self):
        return next(self.points)


class TracePoint(NamedTuple):
    wait: float
    input: str
    parameters: dict[str, Any]


def _prepare_parameter(param, rng):
    if param["type"] == "range_float":
        value = rng.uniform(param["min"], param["max"])
    elif param["type"] == "range_int":
        value = rng.randint(param["min"], param["max"])
    elif param["type"] == "ensemble":
        value = rng.choice(param["values"])
    else:
        raise ValueError(f'unknown parameter type "{param["type"]}"')

    return value


def _get_trace_point(start_time, end_time, iat_gen, action_input_kind, action_params, nb_inputs, rng):
    wait = next(iat_gen) + start_time
    cur_duration = wait
    while cur_duration < end_time:
        if action_input_kind != "none":
            input_id = rng.randint(1, nb_inputs[action_input_kind])
            input_objectname = str(input_id) + "." + INPUT_EXTENSIONS[action_input_kind]
        else:
            input_objectname = None

        params = {param["name"]: _prepare_parameter(param, rng) for param in action_params if param["name"] != "object"}

        yield TracePoint(wait=wait, input=input_objectname, parameters=params)

        try:
            wait = next(iat_gen)
        except StopIteration:
            # we reached the end of our IATs generator before reaching the end of the trace duration-wise
            break

        cur_duration += wait


def _get_action_annotation(action, annotation_key):
    return [annot for annot in action.annotations if annot["key"] == annotation_key][0]["value"]


def build_traces(duration, user_action_specs, memory_profile, nb_inputs, rng):
    traces = []

    for spec in user_action_specs:
        memory = spec["action"].limits.memory

        # preprocessing of arguments to _get_trace_point:
        # * we may have an activity window, otherwise just generate point from 0 to the given duration
        # * inter-arrival times are either given by the spec (key 'times') or generated from a statistical distribution
        points = _get_trace_point(
            spec["activity-window"]["from"] if "activity-window" in spec and spec["activity-window"] is not None else 0,
            (
                min(duration, spec["activity-window"]["to"])
                if "activity-window" in spec and spec["activity-window"] is not None
                else duration
            ),
            (
                iter(spec["times"])
                if "times" in spec
                else IAT_GENERATORS[IatDistribution(spec["distribution"])](spec["rate"], rng)
            ),
            _get_action_annotation(spec["action"], "input_kind"),
            _get_action_annotation(spec["action"], "parameters"),
            nb_inputs,
            rng,
        )

        traces.append(
            Trace(
                spec["name"],
                memory,
                points,
            )
        )

    return traces


def make_injection_traces(
    userspecs: dict[str, dict],
    duration: float,
    action_manifest_fn: str | PathLike,
    traces_dp: str | PathLike,
    rngseed: Any,
):
    funcs = read_manifest(action_manifest_fn)

    rng = Random(rngseed)

    for username, userspec in userspecs.items():
        user_action_specs = userspec["actions"]
        for action_spec in user_action_specs:
            pkg_name, action_name = tuple(action_spec["name"].split("/"))
            action_spec["action"] = funcs[pkg_name][action_name]

        user_traces = build_traces(duration, user_action_specs, userspec["memory-profile"], userspec["nb-inputs"], rng)

        for i, trace in enumerate(user_traces):
            with open(path.join(traces_dp, f"{username}_{i + 1}.faas"), "w") as trace_file:
                trace_file.write("\t".join((username, f"{trace.action_name}", str(trace.action_memory))) + "\n")
                # in the tuples, point.input may be None, and point.parameters may be the empty dict, thus the
                # expression around it to produce None instead
                point_lines = [
                    (
                        str(point.wait),
                        point.input,
                        *(
                            [f"{param_name}:{param_value}" for param_name, param_value in point.parameters.items()]
                            or (None,)
                        ),
                    )
                    for point in trace
                ]
                trace_file.writelines(
                    ["\t".join((elem for elem in line if elem is not None)) + "\n" for line in point_lines]
                )

"""Classes of global configuration for the stresstest experiments.

* experiment: stress-testing FaaSLoad in some way
* experiment run: one repeat of an experiment under certain parameters
* run sample: one sample run, each run has many to measure repeatability
"""

import logging.config
import os
from dataclasses import asdict, dataclass
from logging import FileHandler, Logger
from os import PathLike, path
from typing import Any, cast

import tomlkit

GLOBAL_CONFIG_FN = "global.toml"
LOGGING_CONFIG_FN = "logging.toml"


@dataclass
class ActionConfiguration:
    name: str
    manifest_fp: str
    parameters_fp: str | None

    @classmethod
    def from_toml(cls, table) -> "ActionConfiguration":
        return cls(
            name=table["name"].unwrap(),
            manifest_fp=path.abspath(table["manifest_fp"].unwrap()),
            parameters_fp=path.abspath(fp) if (fp := table.get("parameters_fp")) is not None else None,
        )

    def to_toml(self) -> dict[str, Any]:
        self_d = asdict(self)
        if self.parameters_fp is None:
            del self_d["parameters_fp"]

        return self_d


@dataclass
class OpenWhiskConfiguration:
    namespace: str


@dataclass
class KubernetesConfiguration:
    namespace: str


@dataclass
class FaaSLoadConfiguration:
    repository_dp: str
    loadwskassets_script_fp: str
    runner_script_fp: str
    apihost: str

    @classmethod
    def from_toml(cls, table) -> "FaaSLoadConfiguration":
        return cls(
            repository_dp=path.abspath(table["repository_dp"].unwrap()),
            loadwskassets_script_fp=table["loadwskassets_script_fp"].unwrap(),
            runner_script_fp=table["runner_script_fp"].unwrap(),
            apihost=table["apihost"].unwrap(),
        )


@dataclass
class PathsConfiguration:
    logs: str
    results: str

    @classmethod
    def from_toml(cls, table) -> "PathsConfiguration":
        return cls(
            logs=path.abspath(table["logs"].unwrap()),
            results=path.abspath(table["results"].unwrap()),
        )


@dataclass
class FilenamesConfiguration:
    experiment_parameters: str
    experiment_log: str
    experiment_result: str


@dataclass
class GlobalConfiguration:
    rngseed: Any
    openwhisk: OpenWhiskConfiguration
    kubernetes: KubernetesConfiguration
    faasload: FaaSLoadConfiguration
    paths: PathsConfiguration
    filenames: FilenamesConfiguration

    @classmethod
    def from_toml(cls, table) -> "GlobalConfiguration":
        return cls(
            rngseed=table["rngseed"].unwrap(),
            openwhisk=OpenWhiskConfiguration(**table["openwhisk"].unwrap()),
            kubernetes=KubernetesConfiguration(**table["kubernetes"].unwrap()),
            faasload=FaaSLoadConfiguration.from_toml(table["faasload"]),
            paths=PathsConfiguration.from_toml(table["paths"]),
            filenames=FilenamesConfiguration(**table["filenames"].unwrap()),
        )

    def to_toml(self) -> dict[str, Any]:
        return asdict(self)


def load_global_config(config_dp: str = "config") -> GlobalConfiguration:
    with open(path.join(config_dp, GLOBAL_CONFIG_FN), "r") as config_f:
        return GlobalConfiguration.from_toml(tomlkit.load(config_f))


def configure_logging(config_dp: str = "config"):
    with open(path.join(config_dp, LOGGING_CONFIG_FN), "r") as config_f:
        logging_conf = tomlkit.load(config_f).unwrap()
        logging.config.dictConfig(logging_conf)


def get_experiment_logger(global_config: GlobalConfiguration, expe_name: str, expe_log_dp: str | PathLike) -> Logger:
    logger = logging.getLogger("experiment." + expe_name)
    # TODO replace the machinery of setting the log filename late, with a DynamicFileHandler that lets itself set the
    # filename late (here we use delay on the FileHandler to prevent opening the file before any emit).
    file_handler = cast(FileHandler, logging.getHandlerByName("experiment_log_file"))
    file_handler.baseFilename = path.join(expe_log_dp, global_config.filenames.experiment_log)
    logger.addHandler(file_handler)

    return logger


def make_result_dp(global_config: GlobalConfiguration, expe_name: str, expe_dn: str | PathLike) -> str:
    ret = path.join(global_config.paths.results, expe_name, expe_dn)

    os.makedirs(ret)

    # Update the symlink towards the latest results.
    new_link_fp = path.join(global_config.paths.results, expe_name, "latest.new")
    os.symlink(expe_dn, new_link_fp)
    os.rename(new_link_fp, path.join(global_config.paths.results, expe_name, "latest"))

    return ret


def write_global_config(global_config: GlobalConfiguration, expe_res_dp: str | PathLike) -> str:
    conf_fp = path.join(expe_res_dp, GLOBAL_CONFIG_FN)
    with open(conf_fp, "w") as conf_f:
        tomlkit.dump(global_config.to_toml(), conf_f)

    return conf_fp


def write_experiment_parameters(global_config: GlobalConfiguration, expe_res_dp: str | PathLike, params: Any) -> str:
    params_fp = path.join(expe_res_dp, global_config.filenames.experiment_parameters)
    with open(params_fp, "w") as params_f:
        tomlkit.dump(params.to_toml(), params_f)

    return params_fp


def make_log_dp(global_config: GlobalConfiguration, expe_name: str, expe_dn: str | PathLike) -> str:
    ret = path.join(global_config.paths.logs, expe_name, expe_dn)

    os.makedirs(ret)

    # Update the symlink towards the latest results.
    new_link_fp = path.join(global_config.paths.logs, expe_name, "latest.new")
    os.symlink(expe_dn, new_link_fp)
    os.rename(new_link_fp, path.join(global_config.paths.logs, expe_name, "latest"))

    return ret
